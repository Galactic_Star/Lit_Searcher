///本文件及 example.cpp 中以三杠 /// 开头的注释均为说明用，在实际制作项目时省略
///实际组织顺序可以参照两个template文件（或者直接 Ctrl C  Ctrl V然后去改文件名）
#pragma once
#ifndef _example_h			///用“下划线-文件名”的方法配合 pragma once 实现防止重复include
#define _example_h

//    版    本：	v1.0.0
//    更新日期：	2021-02-06
//    说    明：	本文件实现了对代码风格的说明，没有具体的实现功能


//库包含段开始

#include <iostream>

//库包含段结束

//宏定义段开始

#define TEST_EXAMPLE_DEFINE 32		//代码格式示例中定义此宏为32


//宏定义段结束


//数据类型定义段开始

//代码格式范例中用于存放两个32位整数的结构体
struct Example_Msg_TypeDef
{
	int First = 1;		//第一个整数
	int Second = 2;		//第二个整数
};

//数据类型定义段结束



//全局变量段开始
static int Example_Global = 64;		//代码格式范例中的全局变量


//全局变量段结束

//这个函数只能在一行内从0到input间挨个输出整数，input默认值为10
//只有返回值0表示成功退出
//输入前请检查参数合法性再传入
int Example_Print(int input = 10);


//这个类只在格式示例中用于输出hello world
class Example_Hello {
public:
	//输出一句“hello world”，无返回值
	void Hello();


	Example_Hello();
	~Example_Hello();
private:
	int _Length;	//一个没有意义的长度变量，不会被使用到
};






#endif		// !_example_h