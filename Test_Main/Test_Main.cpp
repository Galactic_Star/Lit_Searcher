#include <iostream>
#include <string>
using namespace std;

unsigned int BKDRHash12(std::string str)
{
	unsigned int seed = 2;
	unsigned int hash = 0;
	int pos = 0;
	int len = str.length();
	while (pos < len)
	{
		hash = (hash << seed) + str[pos];
		pos++;
	}

	return (hash & 0xFFF);
}
unsigned int BKDRHash32(std::string str)
{
	unsigned int seed = 4;
	unsigned int hash = 0;
	int pos = 0;
	int len = str.length();
	while (pos < len)
	{
		hash = (hash << seed) + str[pos];
		pos++;
	}

	return (hash & 0xFFFFFFFF);
}
int main() {
	cout << "欢迎来到数据结构大作业之 文献查询系统！\n这里是测试主程序。\n如果你看到这行字，说明你成功连接到了这一项目！\n";
	cout << "\n开发者打卡状态（按首字母排序）：\n";
	cout << "\tB :( N )\n";
	cout << "\tF :( Y )\n";
	cout << "\tG :( Y )\n";
	cout << "\tL :( N )\n";

	char temp[2000];
	string s;
	while (cin.getline(temp, 2000))
	{
		s = temp;
		printf("%X\n", BKDRHash12(s));
		cout << int(BKDRHash32(s)) << endl << endl;

	}

	system("pause");
	return 0;
}