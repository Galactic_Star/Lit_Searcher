#pragma once
#ifndef _config_h
#define _config_h

//    版    本：	v1.0.1
//    更新日期：	2021-05-07
//    说    明：	各项功能的参数配置文件


//配置交互模式下文件路径缓冲区长度
#define INTERACTIVE_FILE_PATH_MAX_LENGTH 2000

//是否生成类JSON格式的大文件
// 这种大文件可以使用支持随机读写的方法进行访问，访问规则是：
// 假设想要读取的记录序号为X
// 将读指针置于 X<<2 （即四倍X）的位置读取32位（uint32型）得到Y
// 将读指针置于 (X+1)<<2 的位置同样读取32位得到Z
// 设置读指针为Y，读取至 (Z-1) 的位置（包含Z-1）即为目标JSON
//	（ID从1开始，因此文档首的前4字节为记录总数）
#define TRANSFORMAT_GENETART_LARGE_FILE  false




//控制由字符串生成的ID的相关规则

//生成字符串ID的类型
typedef unsigned int Str_ID_TypeDef;
//生成字符串ID时允许的ID最大值（不包括此值）
#define STR_ID_ALLOWED_MAX_ID	0x7FFFFFFF
//生成字符串ID的起始值
#define STR_ID_INIT_NUM			0x1
//非法字符串ID值（如状态错误）
#define STR_ID_FAILED_NUM			0x0


//控制作者统计规则

//论文最多的作者统计多少个
#define AUTHOR_MAX_ATRICLE_NUM 100

//控制文件分片规则（每片文件数，为0不分片
#define FILE_PIECE_NUMBER 1000

#endif		// !_config_h