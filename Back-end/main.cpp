//    版    本：	v0.0.1
//    更新日期：	2021-03-10
//    说    明：	本文件为核心逻辑的入口

#include "main.h"


//错误回滚程序
void rollback()
{

}

//id为类型标识符，规定如下：
// R:文献hash索引
// A:文献内容
// U:作者hash索引
// B:作者名称
// 

//请确保start后面有超过8byte空间
//start为第一个输出位置
inline void printHex(char* start,int value) {
	sprintf_s(start,9, "%X", value);
}

int analysis(const char* source_path, const char* output_path)
{
	//尝试打开文件来判断文件是否存在，如果是，再进入xml解析环节
	FILE* has_file;
	fopen_s(&has_file, source_path, "rb");
	if (has_file == NULL)		//打开失败说明文件不存在
	{
		return -1;
	}
	else
	{
		//前期准备

		fclose(has_file);

		DB_Loader db;
		db.ReLoad_DB(source_path, 0);
		int i = 0;
		DB_item i0;
		clock_t t0 = clock();

		//构建存储空间 开始

		//作者信息为共同的存储空间（id全局唯一）
		//八大类文献信息存储空间也相同（id全局唯一）
		//不同类文献也存储在相同的文件中

		//二重哈希的typedef
		typedef std::unordered_map<unsigned int, Key_Map> dhash_t;
		//二重哈希的pair的typedef
		typedef std::pair<unsigned int, Key_Map> dhash_pt;

		//文献空间
		//当前正在输出的文件指针
		FILE* article_pfile = NULL;
		//当前文件序号
		int article_fileid = 0;
		//当前文件已写入数量
		int article_file_num = 0;
		//文献已写入数量
		//同时为下一条写入的ID
		int article_count = 0;

		//作者ID检索空间
		Str_ID_Mgr author_id;
		//作者统计信息
		Author_Statistician author_info;
		//作者ID - 初级哈希 - 次级哈希 对应表
		//first 为初次哈希值，second为文件内容（等待被导出）
		dhash_t author_hash;

		//文献id及json的对应由本函数手动管理
		//文献ID - 初级哈希 - 次级哈希 对应表
		//first 为初次哈希值，second为文件内容（等待被导出）
		dhash_t article_hash;


		//文献数据输出格式
		const static char article_output_format[] = "{\"type\":\"%s\",%s}";
		//文件名后缀
		const static char json_ext_name[] = ".json";
		//文献详情输出位置
		char path[INTERACTIVE_FILE_PATH_MAX_LENGTH];
		path[0] = 0;


		//空路径认为是指向当前运行路径
		if (!output_path)
		{
			strcpy_s(path, DEFAULT_OUTPUT_PATH);
		}
		else
		{
			strcpy_s(path, output_path);
		}
		//路径检查结尾
		int path_size = strlen(path);
		{
			char i = path[path_size - 1];

			//检查及修正路径结尾
			if ((i != '/') && (i != '\\'))
			{
				char finded = '\\';
				//根据路径格式选择添加啥
				for (int i = 0; i < path_size; ++i)
					if (path[i] == '/')
					{
						finded = '/';
						break;
					}
				path[path_size] = finded;
				++path_size;
				path[path_size] = 0;
			}
		}

		//构建存储空间 结束


		while (db.Good())
		{
			i0 = db.Load_Item();
			++i;

			/*针对每一条已读取记录的处理*/
			//读取结果错误处理
			//视为读取结束
			if (!db.Good())
			{
				break;
			}
			//如果不是而且遇到空串也退掉
			if (i0.type.empty())
			{
				break;
			}

			Str_ID_TypeDef cur_art_id = STR_ID_ALLOWED_MAX_ID;
			
			//写入文献，得到文献位置信息
			{
				//为0需要创建新的文件
				if (article_file_num == 0)
				{
					path[path_size] = 'A';
					printHex(&path[path_size + 1], article_fileid);
					strcat_s(path, json_ext_name);
					//打开文件
					if (fopen_s(&article_pfile, path, "wb") != 0)
					{
						rollback();
						return int(return_TypeDef::path_error);
					}

					//定位到文件头
					fseek(article_pfile, 0, SEEK_SET);
					fprintf_s(article_pfile, "[");
					path[path_size] = 0;
				}//！为0需要创建新的文件

				//可能需要写分隔符
				if (article_file_num)
				{
					fprintf_s(article_pfile, ",");
				}
				//核心写入
				{
					//更新当前操作ID和文件总数
					cur_art_id = article_count;
					++article_count;
					++article_file_num;
					//输出内容
					fprintf_s(article_pfile, article_output_format, i0.type.c_str(), i0.json.c_str());
					i0.type.clear();
					i0.type.shrink_to_fit();
					i0.json.clear();
					i0.json.shrink_to_fit();
					fflush(article_pfile);
				}//!核心写入


//判断是否输入达分片限制
				{
					if (article_file_num >= FILE_PIECE_NUMBER)
					{
						fprintf_s(article_pfile, "]");
						article_file_num = 0;
						++article_fileid;
						fclose(article_pfile);
						article_pfile = NULL;
					}
				}//!判断是否输入达分片限制

			}//！写入文献，得到文献位置信息
			
			//处理文献名索引
			{
				unsigned int prim_hash = 0;
				//hash依据
				std::string refer;
				//对于非www条目，优先级：title author type
				//对于www条目，优先级：author title type
				if (i0.type == "www")
				{
					if (i0.authors.size())
						refer = (*(i0.authors.begin()));
					else if (i0.name.size())
						refer = (i0.name);
					else
						refer = (i0.type);
				}
				else
				{
					if (i0.name.size())
						refer = (i0.name);
					else if (i0.authors.size())
						refer = (*(i0.authors.begin()));
					else
						refer = (i0.type);
				}
				prim_hash = BKDRHash12(refer);


				//查看此初级hash是否已有对应文件（key_map)，没有则先创建
				if (article_hash.find(prim_hash) == article_hash.end())
				{
					Key_Map t;
					article_hash.insert(dhash_pt(prim_hash, t));
				}
				//把ID加入
				article_hash.find(prim_hash)->second.Add_ID(BKDRHash32(refer), cur_art_id);

			}//!处理文献名索引

			//处理作者信息
			//if(0)
			{
				Str_ID_TypeDef taid = STR_ID_FAILED_NUM;	//存放临时的作者ID
				std::vector<Str_ID_TypeDef> aid;	//查询到的作者id数组

				//扫描作者string得到一组作者id，同时判断是否需要生成 hash
				for (std::list<std::string>::iterator it = i0.authors.begin();
					it != i0.authors.end(); ++it)
				{
					char rst = author_id.Get_Str_ID(*it, taid);
					//如果出错退出至错误处理程序
					if ((rst < 0) || (taid == STR_ID_FAILED_NUM))
					{
						rollback();
						return int(return_TypeDef::unexpected_error);
					}
					//查到id，但显示为新id，则应当加入其 hash 值
					if (rst == 0)
					{
						//生成初级hash
						unsigned int prim_hash = BKDRHash12(*it);
						//查看此初级hash是否已有对应文件（key_map)，没有则先创建
						if (author_hash.find(prim_hash) == author_hash.end())
						{
							Key_Map t;
							author_hash.insert(dhash_pt(prim_hash, t));
						}
						//把ID加入
						author_hash.find(prim_hash)->second.Add_ID(BKDRHash32(*it), taid);
					}
					//存放ID
					aid.push_back(taid);
				}//!扫描作者string得到一组作者id，同时判断是否需要生成 hash

				//如果作者列表非空：
				//如果非www，直接加入合作库
				//如果是www，分别加入合作库（加入文章记录但不加合作者）
				if (aid.size())
				{
					if (i0.type != "www")
						author_info.Add_Record(aid, cur_art_id);
					else
					{
						for (int i = 0; i < aid.size(); ++i)
							author_info.Add_Record(std::vector<Str_ID_TypeDef>(aid[i]), cur_art_id);
					}
				}
			}//!处理作者信息

			i0.authors.clear();
			i0.json.clear();
			i0.name.clear();
			i0.type.clear();

			//显示扫描结果
			if (!(i & 0x1FFF))
			{
				system("cls");
				printf("已分析数量：0x%X\n", i);
				printf("已分析文件大小：%d MB\n\n", ((db.Position())>>20));
				printf("用时:  %d s\n\n", (clock() - t0) / 1000);
				printf("Articles:%d\nBook:%d\nInproceedings:%d\nMastersthesis:%d\nPhdthesis:%d\nProceedings:%d\nWWWs:%d\nIncollections:%d\nauthors:%d\n",
					db.Articles(), db.Book(), db.Inproceedings(),
					db.Mastersthesises(), db.Phdthesises(),
					db.Proceedings(), db.WWWs(), db.Incollections(), db.Authors());
			}
		}

		/*读取结束的尾处理*/

		system("cls");
		printf("分析已结束。\n\n已分析数量：0x%X\n", i);
		printf("用时:  %d s\n\n", (clock() - t0) / 1000);
		printf("Articles:%d\nBook:%d\nInproceedings:%d\nMastersthesis:%d\nPhdthesis:%d\nProceedings:%d\nWWWs:%d\nIncollections:%d\nauthors:%d\n",
			db.Articles(), db.Book(), db.Inproceedings(),
			db.Mastersthesises(), db.Phdthesises(),
			db.Proceedings(), db.WWWs(), db.Incollections(), db.Authors());

		//关闭内容文件
		if (article_file_num && article_pfile)
		{
			fprintf_s(article_pfile, "]");
			fclose(article_pfile);
			article_pfile = NULL;
		}

		//导出作者ID
		printf("\n开始导出作者映射表...\n");
		return_TypeDef ret = author_id.Export_JsonResult(output_path, "B", "json", FILE_PIECE_NUMBER);
		if (ret	!= return_TypeDef::normal)
		{
			printf("\n作者映射表导出出错，错误代码%d\n",int(ret));
			rollback();
			return -2;
		}
		printf("作者映射表导出完成\n");
		//导出作者哈希
		printf("\n开始导出作者索引表...\n");
		{
			char temp[16] = "U";
			for (dhash_t::iterator it = author_hash.begin(); it != author_hash.end(); ++it)
			{
				printHex(temp + 1, it->first);
				it->second.Export_JsonResult(output_path, temp, "json");
			}
		}
		printf("作者索引表导出完成\n");
		printf("\n开始导出文献索引表...\n");
		//导出文献哈希
		{
			char temp[16] = "R";
			for (dhash_t::iterator it = article_hash.begin(); it != article_hash.end(); ++it)
			{
				printHex(temp + 1, it->first);
				it->second.Export_JsonResult(output_path, temp, "json");
			}
		}
		printf("文献索引表导出完成\n");
		printf("\n开始导出作者统计信息...\n");
		//导出作者统计关系
		{
			return_TypeDef ret = author_info.Export_JsonResult(output_path, "C", "json", FILE_PIECE_NUMBER);
			if (ret != return_TypeDef::normal)
			{
				printf("\n作者统计信息导出出错，错误代码%d\n", int(ret));
				rollback();
				return -2;
			}
			
		}
		printf("作者统计信息导出完成\n");
		printf("\n开始统计信息...\n");

		//生成统计信息
		{
			FILE* target = NULL;
			path[path_size] = 0;
			strcat_s(path, "info.json");
			if (fopen_s(&target, path, "wb") != 0)
				return int(return_TypeDef::path_error);
			//定位到文件头
			fseek(target, 0, SEEK_SET);
			//写入 info 信息
			fprintf_s(target,
				"{\"filecount\":%d,\n\"authorcount\":%d,\n\"offset\":%d,\n\"pieces\":%d,\n",
				article_count,
				author_id.size(),
				STR_ID_INIT_NUM,
				FILE_PIECE_NUMBER);

			fprintf_s(target,
				"\"Articles\":%d,\n\"Book\":%d,\n\"Inproceedings\":%d,\n\"Mastersthesis\":%d,\n\"Phdthesis\":%d,\n\"Proceedings\":%d,\n\"WWWs\":%d,\n\"Incollections\":%d,\n\"authorfreq\":%d}",
				db.Articles(), db.Book(), db.Inproceedings(),
				db.Mastersthesises(), db.Phdthesises(),
				db.Proceedings(), db.WWWs(), db.Incollections(), db.Authors());

			//关闭 info
			fclose(target);
			target = NULL;
			printf("统计信息导出完成\n\n");

		}

		return 0;
	}
}

int pre_analysis(const char* source_path)
{
	//尝试打开文件来判断文件是否存在，如果是，再进入xml解析环节
	FILE* has_file;
	fopen_s(&has_file,source_path, "r");
	if (has_file == NULL)		//打开失败说明文件不存在
	{
		return -1;
	}
	else
	{
		//关闭以便重新打开
		fclose(has_file);

		DB_Loader db;
		db.ReLoad_DB(source_path,0);
		int i = 0;
		DB_item i0;
		clock_t t0 = clock();
		//主循环
		while (db.Good())
		{
			i0 = db.Load_Item();
			//控制详细信息输出
			if (0)
			{
				printf("type:\t");
				printf(i0.type.c_str());
				printf("\n\njson:\t");
				printf(i0.json.c_str());
				printf("\n\nname:\t");
				printf(i0.name.c_str());
				printf("\n\nauthors:\t");
				std::list<std::string>::iterator it = i0.authors.begin();
				for (; it != i0.authors.end(); ++it)
				{
					printf(it->c_str());
					printf("\n");
				}

				printf("year:\t%d\n", i0.year);
				printf("\n\n");
			}


			++i;
			if (!(i & 0xFFF))
			{
				system("cls");
				printf("已分析数量：0x%X\n", i);
			}
		}
		//输出分析结果
		printf("\nall finished \n\ntime use:  %d ms\n\n", clock() - t0);
		printf("Articles:%d\nBook:%d\nInproceedings:%d\nMastersthesis:%d\nPhdthesis:%d\nProceedings:%d\nWWWs:%d\nIncollections:%d\nauthors:%d\n",
			db.Articles(), db.Book(), db.Inproceedings(),
			db.Mastersthesises(), db.Phdthesises(),
			db.Proceedings(), db.WWWs(), db.Incollections(), db.Authors());
		return 0;
	}
}