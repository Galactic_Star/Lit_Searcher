#pragma once
#ifndef _hash_2_0_h
#define _hash_2_0_h

//    版    本：	v1.0.0
//    更新日期：	2021-05-11
//    说    明：	本文件实现了数个哈希函数，生成指定长度的哈希值，
//					与网页端调用的 hash 2.0.js 保持同步。
//					函数及算法由 F 实现，此后由 G 整理头文件。


//库包含段开始
#include<iostream>
#include<string>
//库包含段结束


//宏定义段开始
//宏定义段结束


//数据类型定义段开始
//数据类型定义段结束


//全局变量段开始
//全局变量段结束



// BKDR Hash Function
unsigned int BKDRHash(std::string str);

//生成 8bit 长度的哈希值
unsigned int BKDRHash8(std::string str);

//生成 12bit 长度的哈希值
unsigned int BKDRHash12(std::string str);

//生成 16bit 长度的哈希值
unsigned int BKDRHash16(std::string str);

//生成 32bit 长度的哈希值
unsigned int BKDRHash32(std::string str);


#endif		// !_hash_2_0_h