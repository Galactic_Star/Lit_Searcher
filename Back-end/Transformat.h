#pragma once
#ifndef _Transformat_h
#define _Transformat_h

//    版    本：	v0.2.0
//    更新日期：	2021-05-09
//    说    明：	本文件实现了由DBLP的xml格式数据库转存为json格式的能力。
//					使用 Load_DB() 打开文件并自动过滤头部，如果已打开过则重新打开
//					使用 Reload_DB() 打开文件并定位到上一次读写位置，传入文件位置及get指针
//					使用 Load_Item() 加载一条条目并自动转为JSON格式多个类型的关键分析结果
//
//					


//库包含段开始
#include <string>
#include <list>
#include <vector>
#include <fcntl.h>
#include "defines.h"
#include "config.h"
//库包含段结束


//宏定义段开始
//宏定义段结束


//数据类型定义段开始
//数据类型定义段结束


//全局变量段开始

//这个结构体存储分析结果的JSON字符串(std::string)及部分额外信息
// 额外信息包括类型(std::string)年份（unsigned short)、书名(std::string)及作者列表(std::list<std::string>)
struct DB_item
{
	std::string json;					//JSON本体
	std::string type;					//文献类型（如article）
	unsigned short year = 0;			//年份信息
	std::string name;					//文献名
	std::list<std::string> authors;		//作者列表
};

//全局变量段结束



class DB_Loader {
public:

	//默认初始化函数，可传入文件路径自动打开文件并过滤掉头部
	DB_Loader(const char* const path = NULL);
	virtual ~DB_Loader();

	//修改复制函数禁用复制（复制出空节点）
	DB_Loader(DB_Loader& old);

	DB_Loader operator=(DB_Loader& old);


	//	使用 Load_DB() 打开文件并自动过滤头部
	//  参数：
	// 	   path   文件路径，可以为相对路径或绝对路径
	// 	返回值：
	//	   正常打开				return_TypeDef::normal
	//	   路径错误无法打开		return_TypeDef::input_path_error
	//	   认为非DBLP数据库		return_TypeDef::not_a_dblp_db
	return_TypeDef Load_DB(const char* const path);


	//	使用 Reload_DB() 打开文件并定位到上一次读写位置，传入文件位置及get指针
	//  参数：
	// 	   path   文件路径，可以为相对路径或绝对路径
	// 	   pos	  上次已读取位置， int32型
	// 	返回值：
	//	   正常打开				return_TypeDef::normal
	//	   路径错误无法打开		return_TypeDef::input_path_error
	//	   认为非DBLP数据库		return_TypeDef::not_a_dblp_db
	//	   认为读指针错误		return_TypeDef::input_ptr_error
	return_TypeDef ReLoad_DB(const char* const path, int pos = 0);


	//	使用 Load_Item() 加载一条条目并自动转为JSON格式返回一个 DB_item 对象
	// 	对于存在属性的纯文本节点，文本保存于value属性中
	//  参数：
	// 	   无
	// 	返回值：
	//	   正常读取						返回值.type为非空串
	//	   数据库已结束或遇到错误		返回值.type为空串
	DB_item Load_Item(void);

	//判断是否可以正常读写
	int Good(void);

	//获取已识别文章数
	int Articles(void);

	//获取已识别书数
	int Book(void);

	//获取已识别Inproceeding数
	int Inproceedings(void);

	//获取已识别Incollection数
	int Incollections(void);

	//获取已识别Mastersthesis数
	int Mastersthesises(void);

	//获取已识别Phdthesis数
	int Phdthesises(void);

	//获取已识别Proceeding数
	int Proceedings(void);

	//获取已识别主页数
	int WWWs(void);
	
	//获取已识别作者人次
	int Authors(void);

	//获取当前文件指针（下一读取位置）
	int Position(void);


private:


	//	使用 Load_Head() 过滤掉xml中的文件头部，或过滤到适合开始读取的位置（以<article>标签为界）
	//	成功返回1，否则返回0
	char _Load_Head(void);

	//用于递归加载
	//  name为当前读取的 Object 名字（不包括引号）。如果遇到关闭标签（如/article）则此处为空串
	// 	value 为其值的json格式。如不为数组、Object的普通类型，（比如是number或string）则不包括双引号
	//			如为数组或对象，也不包括两侧括号（便于合并）
	//			纯文本节点带双引号和反斜杠保护（会转换为 \" \\ ）
	// 	authors 为特殊节点值下的字符串链表（均带双引号和反斜杠保护）
	// 	   当标签为 author ，每个作者为1条目
	// 	title 为特殊节点值下的字符串链表（带双引号和反斜杠保护）
	// 	   当标签为 title ，仅有一条条目
	//返回值规定为
	// 	   0 未读取到值（遇到关闭标签）（如遇到关闭标签会将其消耗完）
	// 	   1 普通类型
	//	   2 数组
	//	   3 object
	//DBLP的相同标签相邻存放
	int _Seek_Obj(std::string& name, std::string& value, std::list<std::string>& specify, std::string& title, unsigned short& year, char recu = 1);

	//过滤掉空白字符，包括 \r \t \n 空格
	void _Filt_Empty(void);

	//试探下一个字符
	char _Fseekc();

	//匹配tag，传入时应在 < 前（允许有空字符）
	// 匹配成功则读完 > 或回退（roll_back为非零）
	// 匹配失败则回退至调用前的状态
	//返回值：
	// 	   1： author
	//	   2： title
	// 	   3： year
	//	   0： 其他 
	char _Tag_Match(char roll_back);

	//探测下一标签内容并返回（不包括 <>）
	// 匹配成功则读完 > 或回退（roll_back指定）
	// 	   0：不回退
	// 	   1：遇到关闭标签时不回退，开始标签时回退
	// 	   2：回退
	std::string _Seek_Tag(char roll_back);

	//当前操作的数据库
	FILE* _pDB;

	//上一次读取出错标记，为 true 时表示上一次读取出错， Good() 返回0
	char _Last_Failed;

	//统计信息
	unsigned int _nArticle;				//文献次数
	unsigned int _nBook;				//书本次数
	unsigned int _nInproceeding;		//次数
	unsigned int _nMastersthesis;		//次数
	unsigned int _nPhdthesis;			//次数
	unsigned int _nProceedings;			//次数
	unsigned int _nWWW;					//主页数
	unsigned int _nIncollection;		//Incollection数

	unsigned int _nAuthor;		//作者人次
};







#endif		// !_Transformat_h