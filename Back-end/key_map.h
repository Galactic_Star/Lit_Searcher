#pragma once
#ifndef _key_map_h
#define _key_map_h

//    版    本：	v0.1.0
//    更新日期：	2021-05-13
//    说    明：	本文件用于 ID 与 Hash值的绑定关系查询，仅绑定一层哈希。
//					使用 Key_Map 访问
//					使用STL unordered_map做查询实现，key为哈希值，value为ID数组。
//					哈希方法由外部提供，仅传入哈希值。
//					使用 Add_ID(...) 增加记录（如已存在不会重复添加）
//					使用 Export_JsonResult(...)  方法可以导出JSON格式结果至指定路径
//					使用 Reinit()清空已保存的键值对并重新初始化
// 建设中


//库包含段开始
#include <unordered_map>
#include <string>
#include <vector>
#include <fcntl.h>
#include "defines.h"
#include "config.h"
//库包含段结束


//宏定义段开始
//宏定义段结束


//数据类型定义段开始
typedef unsigned int kmHash_TypeDef;
typedef unsigned int kmID_TypeDef;
//数据类型定义段结束


//全局变量段开始
//全局变量段结束


class Key_Map
{
public:
	Key_Map();
	Key_Map(const Key_Map&);
	~Key_Map();

	//主要用于复制
	Key_Map& operator= (const Key_Map&);

	//清空已保存的键值对并重新初始化
	void Reinit();

	//为某一哈希添加id至表中。若id与哈希均相同则不添加
	//成功添加（包括已存在不添加）返回 normal
	//状态错误返回state_error
	return_TypeDef Add_ID(kmHash_TypeDef, kmID_TypeDef id);

	//查询哈希值为 code 下的id数量
	int Ask_ID_size(kmHash_TypeDef code);

	//清空 sto 并将哈希值为 code 的所有id写入sto中，因此如查无哈希值sto变为空vector
	//返回写入的 id数量
	int Get_ID(kmHash_TypeDef code, std::vector<kmID_TypeDef>& sto);


	//向目标位置写出一个（一组）JSON文件，如有行尾则为 \n ，文件包括两端 {} （导出为对象）
	//参数说明：
	//	path	 ： 导出文件（组）的路径（文件夹名称）， 可由 \ / 结束或不带（自动添加）
	// 	filename ： 文件名称，如果启用分片则为分片前缀名称（可以为空）
	//	ext_name ： 扩展名名称（不带 . ）。不启用分片时与filename不可同时为空
	// 	line    ： 每一字符串后换行（不推荐，默认关闭）。非零时启用
	// 
	//返回值：
	//	正常创建时返回 normal 
	// 	如果没有可以导出的键值对（列表为空）返回 str_id_no_output
	// 	路径错误 返回 path_error
	//	状态错误 返回 state_error
	//	其余未定义错误 返回 unexpected_error
	return_TypeDef Export_JsonResult(std::string path, std::string filename, std::string ext_name, int line = 0);


private:
	char _good;
	std::unordered_map<kmHash_TypeDef, std::vector<kmID_TypeDef>> _maps;

	int wrong_para(
		std::string& path,
		const std::string& filename,
		const std::string& ext_name);
};







#endif		// !_key_map_h