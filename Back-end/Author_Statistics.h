#pragma once
#ifndef _Author_Statistics_h
#define _Author_Statistics_h


//    版    本：	v0.0.1
//    更新日期：	2021-05-14
//    说    明：	本文件实现了作者统计信息。依赖于Str_ID_Mgr生成的ID（由外部管理ID）
//					使用类 Author_Statistician 可访问以下所有功能
//					作者论文数量统计
//					关联作者关系维护
//					使用邻接表形式实现（哈希+哈希）
//					
// 建设中


//库包含段开始
#include <string>
#include <fcntl.h>
#include <unordered_map>
#include <set>
#include <vector>
#include "config.h"
#include "defines.h"
//库包含段结束


//宏定义段开始
//宏定义段结束


//数据类型定义段开始
//数据类型定义段结束


//全局变量段开始
//全局变量段结束


class Author_Statistician
{
public:
	Author_Statistician();
	Author_Statistician(Author_Statistician&);
	~Author_Statistician();

	Author_Statistician& operator=(Author_Statistician&);

	//清空已保存的作者信息并重新初始化
	void Reinit();

	// 为一组文件作者信息添加至表中，使用std::vector组织作者ID传入
	// 作者未记录则会自动记录作者
	// 成功添加返回 normal
	// 状态错误返回state_error
	// 会对authos进行同ID检测，但不对id作合法性检查
	return_TypeDef Add_Record(std::vector<Str_ID_TypeDef>authors,Str_ID_TypeDef article_id);

	//查询指定 id 下的作者的文献数量
	//作者未记录则会返回0
	int Ask_articleNum(const Str_ID_TypeDef id);

	//清空 sto 并将指定 id 下的作者参与的所有文献id写入sto中，因此如查无ID则sto变为空vector
	//返回写入的 id数量
	int Get_Article(const Str_ID_TypeDef id, std::vector<Str_ID_TypeDef>& sto);

	//查询指定 id 下的作者的合作者数量
	//作者未记录则会返回0
	int Ask_cooperatorNum(const Str_ID_TypeDef id);

	//清空 sto 并将指定 id 下的作者的合作者的所有id写入sto中，因此如查无ID则sto变为空vector
	//返回写入的 id数量
	int Get_Cooperator(Str_ID_TypeDef id, std::vector<Str_ID_TypeDef>& sto);


	//向目标位置写出一个（一组）JSON文件，如有行尾则为 \n ，文件包括两端 [] （导出为数组）
	//参数说明：
	//	path	 ： 导出文件（组）的路径（文件夹名称）， 可由 \ / 结束或不带（自动添加）
	// 	filename ： 文件名称，如果启用分片则为分片前缀名称（可以为空）
	//	ext_name ： 扩展名名称（不带 . ）。不启用分片时与filename不可同时为空
	// 	pieces	 ： 分片大小。为0时关闭分片，否则为每一分片大小（如果大于当前ID最大值依旧以分片模式创建）
	// 	line    ： 每一字符串后换行（不推荐，默认关闭）。非零时启用
	// 
	//返回值：
	//	正常创建时返回 normal 
	// 	如果没有可以导出的关系对（列表为空）返回 str_id_no_output
	// 	路径错误 返回 path_error
	//	状态错误 返回 state_error
	//	其余未定义错误 返回 unexpected_error
	return_TypeDef Export_JsonResult(std::string path, std::string filename, std::string ext_name, int pieces = 0, int line = 0);


private:

	//存放作者相关信息结构体
	//由于作者ID已体现于哈希表key中因此不重复保存
	struct _Author_TypeDef
	{
		std::vector<Str_ID_TypeDef> cooperations;	//合作者ID集
		std::vector<Str_ID_TypeDef> articles;		//文献ID集
	};

	//邻接VE图
	//first为作者ID
	//second为作者相关信息
	std::unordered_map<Str_ID_TypeDef, _Author_TypeDef> _authors;

	//便于方法访问迭代器
	typedef std::unordered_map<Str_ID_TypeDef, _Author_TypeDef> map_typedef;

	//状态机
	char _good;

	//最大作者id，用于分片输出
	Str_ID_TypeDef _max_id;

	int wrong_para(
		std::string& path,
		const std::string& filename,
		const std::string& ext_name,
		const int& pieces);

	//最大的 AUTHOR_MAX_ATRICLE_NUM 个id的处理，
	//输出样式为   maxid:[...],maxnum:[...]，首尾无换行
	//返回值为扫描遇到的最大作者ID
	void write_max_au(FILE* target, int line = 0);

	void insert(std::vector<Str_ID_TypeDef>& target, const Str_ID_TypeDef& value);
};







#endif          //!_Author_Statistics_h