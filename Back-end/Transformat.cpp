//库包含段开始，一般在头文件设置其他include
#include "Transformat.h"

//库包含段结束

//文件内宏定义段开始
//文件内宏定义段结束


//文件内全局变量段开始
//文件内全局变量段结束


DB_Loader::DB_Loader(const char* const path)
{
	_pDB = NULL;
	_nArticle = _nBook = _nInproceeding = _nMastersthesis
		= _nPhdthesis = _nProceedings = _nWWW = _nIncollection = 0;
	_nAuthor = 0;
	_Last_Failed = 0;
	//如果有则打开文件
	if (path)
	{
		Load_DB(path);
	}
}
DB_Loader::~DB_Loader()
{
	if (_pDB)
	{
		fclose(_pDB);
		_pDB = NULL;
	}
}

DB_Loader::DB_Loader(DB_Loader& old)
{
	_pDB = NULL;
	_pDB = NULL;
	_nArticle = _nBook = _nInproceeding = _nMastersthesis
		= _nPhdthesis = _nProceedings = _nWWW = _nIncollection = 0;
	_nAuthor = 0;
	_Last_Failed = 0;
}

DB_Loader DB_Loader::operator=(DB_Loader& old)
{
	_pDB = NULL;
	_nArticle = 0;
	_nAuthor = 0;
	return *this;
}

return_TypeDef DB_Loader::Load_DB(const char* const path)
{
	return ReLoad_DB(path, 0);
}

return_TypeDef DB_Loader::ReLoad_DB(const char* const path, int pos)
{
	//如果打开过就关掉
	if (_pDB)
	{
		fclose(_pDB);
	}
	//重新初始化
	_pDB = NULL;
	_nArticle = _nBook = _nInproceeding = _nMastersthesis
		= _nPhdthesis = _nProceedings = _nWWW = _nIncollection = 0;
	_nAuthor = 0;
	_Last_Failed = 0;
	//如果没有路径则认为出错
	if (!path)
	{
		return return_TypeDef::input_path_error;
	}
	else
	{
		//成功打开则按指定起始位置过滤头部
		if (fopen_s(&_pDB, path, "rb") == 0)
		{
			//设置指针时出现了错误代码
			if (fseek(_pDB, pos, SEEK_SET))
			{
				fclose(_pDB);
				_pDB = 0;
				return return_TypeDef::input_ptr_error;
			}
			//如果没有找到合适的入点则放弃打开
			if (!_Load_Head())
			{
				fclose(_pDB);
				_pDB = NULL;
				//返回错误代码
				return return_TypeDef::not_a_dblp_db;
			}
			else
			{
				return return_TypeDef::normal;
			}
		}
		else
		{
			//打不开认为路径错误
			_pDB = NULL;
			return return_TypeDef::input_path_error;
		}
	}
}
DB_item DB_Loader::Load_Item(void)
{
	DB_item ret;
	_Seek_Obj(ret.type, ret.json, ret.authors, ret.name,ret.year,0);
	if (ret.type.size())
	{
		//分类统计
		if(ret.type =="article")
			++_nArticle;
		else 
		if(ret.type =="book")
			++_nBook;
		else 
		if(ret.type =="inproceedings")
			++_nInproceeding;
		else 
		if(ret.type =="incollection")
			++_nIncollection;
		else 
		if(ret.type =="mastersthesis")
			++_nMastersthesis;
		else 
		if(ret.type =="phdthesis")
			++_nPhdthesis;
		else 
		if(ret.type =="proceedings")
			++_nProceedings;
		else 
		if(ret.type =="www")
			++_nWWW;
		else
		{
			_Last_Failed = 1;
			return DB_item();
		}
		_nAuthor += ret.authors.size();
	}
	return ret;
}


int DB_Loader::Good(void)
{
	if (_Last_Failed)
		return 0;
	return _pDB != NULL;
}

//获取已识别文章数
int DB_Loader::Articles(void)
{
	return _nArticle;
}

int DB_Loader::Book(void)
{
	return _nBook;
}

//获取已识别Inproceeding数
int DB_Loader::Inproceedings(void)
{
	return _nInproceeding;
}

//获取已识别Mastersthesis数
int DB_Loader::Mastersthesises(void)
{
	return _nMastersthesis;
}

//获取已识别Phdthesis数
int DB_Loader::Phdthesises(void)
{
	return _nPhdthesis;
}

//获取已识别Proceeding数
int DB_Loader::Proceedings(void)
{
	return _nProceedings;
}

//获取已识别主页数
int DB_Loader::WWWs(void)
{
	return _nWWW;
}

//获取已识别Incollection数
int DB_Loader::Incollections(void)
{
	return _nIncollection;
}

//获取已识别作者数
int DB_Loader::Authors(void)
{
	return _nAuthor;
}

int DB_Loader::Position(void)
{
	return ftell(_pDB);
}

const static char __article[] = { 'a','r','t','i','c','l','e' };


char DB_Loader::_Load_Head(void)
{
	//判断有文件打开
	if (_pDB)
	{
		//读取缓冲
		char tempc = 0;
		//循环检测每个字符
		while (1)
		{
			tempc = fgetc(_pDB);
			//判断是否到文件尾，是则关闭文件并释放文件指针
			if (tempc == EOF)
			{
				fclose(_pDB);
				_pDB = NULL;
				return 0;
			}
			// < 出现进入标签判断
			if (tempc == '<')
			{
				char success_flag = 1;
				long ft = ftell(_pDB);
				for (char i = 0; i < sizeof(__article); ++i)
				{
					char c = fgetc(_pDB);
					//出现不匹配
					if (c != __article[i])
					{
						success_flag = 0;
						break;
					}
				}
				//识别成功，回退至标签开始
				if (success_flag)
				{
					fpos_t cur = 0;
					fseek(_pDB, ft-1, SEEK_SET);
					ft = ftell(_pDB);
					break;
				}
				//识别失败，继续等待下一 < 出现
				else
				{
					//无需操作
				}
			}
		}
		
	}
	return 1;
}

int DB_Loader::_Seek_Obj(std::string& name, std::string& value, std::list<std::string>& specify, std::string& title, unsigned short& year,char recu)
{
	//找标签界中的 <
	while (fgetc(_pDB) != '<');
	//复位
	name.clear();
	value.clear();
	{
		std::string t;
		t.swap(name);
	}
	{
		std::string t;
		t.swap(value);
	}

	char nodetype = 1;	//遇到 > 为1，遇到空格（需要读属性）为2

	//读取下一字符直到遇到 > 或空格
	{
		char not_meet_end = 1;
		char tagc = 0;
		while (not_meet_end)
		{
			tagc = fgetc(_pDB);
			if (tagc == '>')
			{
				not_meet_end = 0;
				break;
			}
			else if (tagc == ' ')
			{
				not_meet_end = 0;
				nodetype = 2;
				break;
			}
			name += tagc;
		}
	}
	
	//判断标签是否为关闭标签
	if (name.at(0) == '/')
	{
		name.erase();
		//还有没读完的属性要消耗掉
		if (nodetype == 2)
			while (fgetc(_pDB) != '>');
		return 0;
	}
	//可以确认是开始标签了
	

	//检查是否需要读属性
	if (nodetype == 2)
	{
		//逐个读取属性，最后一个属性后方无 逗号 ,
		char state = 1,
			not_stop = 1;//如果为1则为第一次开始，超过1则为第二次开始

		while (not_stop)
		{
			_Filt_Empty();
			//判断是否需要加逗号
			if (not_stop > 1)
				value += ',';

			//读属性名
			value += '\"';
			while (state)
			{
				state = fgetc(_pDB);
				if (state == '=')
					break;
				value += state;
			}

			value += "\":\"";
			//消耗掉一个 "
			fgetc(_pDB);
			//读属性值
			state = 1;
			while (state)
			{
				state = fgetc(_pDB);
				//意外遇到标识符尾直接报错退出
				if (state == '>')
				{
					not_stop = 0;
					{
						std::string t;
						t.swap(name);
					}
					{
						std::string t;
						t.swap(value);
					}
					return 0;
					break;
				}
				value += state;
				if (state == '\"')
					break;
			}

			++not_stop;

			//尝试是否遇到属性尾
			if (fgetc(_pDB) == '>')
			{
				not_stop = 0;
				break;
			}
			else {
				//非属性尾这里是空格，不需要回退
			}
		}
	}

	_Filt_Empty();

	//判断在XML中是否为纯文本节点,title与author、booktitle 必为纯文本节点
	if ((_Fseekc() != '<')||(name == "title")||(name =="author")||(name =="booktitle") || (name == "editor"))
	{
		//是否需要补充分隔符
		if (nodetype == 2)
		{
			value += ",\"value\":\"";
		}
		//读取值
		std::string val;
		//嵌套开始标签层数
		int sub = 1;
		while (sub)
		{
			char temp = 0;
			while ((temp = fgetc(_pDB)) != '<')
			{
				if (temp == '\"')
					val += "\\\"";
				else if (temp == '\\')
					val += "\\\\";
				else
					val += temp;
			}
			//val此时两端仍然没有引号
			
			//检查下一个标签是啥
			if (fgetc(_pDB) == '/')
			{
				//是关闭标签
				--sub;
			}
			else
			{
				++sub;
				while (fgetc(_pDB) != '>');
			}
		}

		//消耗掉关闭标签
		while (fgetc(_pDB) != '>');


		//判断是否为特殊元素（需要追加到特殊值列表）
		if (name == "author")
		{
			specify.push_back(val);
		}
		else if (name == "title")
			title = val;
		else if (name == "year")
			year = atoi(val.c_str());
		else if ((name == "booktitle") && (!title.size()))
			title = val;

		//将文本值写回value 中
		value += val;
		//如果包含属性则需要追加 "
		if (nodetype == 2)
		{
			value += '\"';
		}
	}
	//不为纯文本节点，判断是否遇到关闭标签
	else 
	{
		//没有遇到关闭标签
		//开始读取值（向下生长部分，调用挂载子节点）
		while (_Seek_Tag(1).at(0) != '/')
		{
			std::string sub_name, sub_value;
			switch (_Seek_Obj(sub_name,sub_value,specify,title,year))
			{
			case 1:
			{
				if (nodetype == 2)
					value += ',';
				value = value + '\"' + sub_name + "\":\"" + sub_value + '\"';
				nodetype = 2;
				break;
			}
			case 2:
			{
				if (nodetype == 2)
					value += ',';
				value = value + '\"' + sub_name + "\":[" + sub_value + ']';
				nodetype = 2;
				break;
			}
			case 3:
			{
				if (nodetype == 2)
					value += ',';
				value = value + '\"' + sub_name + "\":{" + sub_value + '}';
				nodetype = 2;
				break;
			}
			default:
				break;
			}
		}
	}
	//看是否允许横向生长
	if(recu)
	//遇到关闭标签则准备横向生长
	//判断下一个有无同名节点，有则应该递归（横向生长部分）
		if (_Seek_Tag(2) == name)
		{
			//将value改为适合数组的形式
			if (nodetype == 1)
				value = '\"' + value + '\"';
			else
				value = '{' + value + '}';

			//进行递归
			std::string bro_name, bro_value;
			switch (_Seek_Obj(bro_name, bro_value, specify, title, year))
			{
				//合并递归结果
			case 1:
				value = value + ",\"" + bro_value + '\"';
				break;
			case 2:
				value = value + ',' + bro_value;
				break;
			case 3:
				value = value + ",{" + bro_value + '}';
				break;
			default:
				break;
			}
			//节点属性返回
			return 2;
		}
	//节点属性返回
	if (nodetype == 2)
		return 3;
	return 1;
}

void DB_Loader::_Filt_Empty(void)
{
	char t = 0;
	do
		t = fgetc(_pDB);
	while ((t == ' ') || (t == '\r') || (t == '\n') || (t == '\t'));
	fseek(_pDB, -1, SEEK_CUR);
}

char DB_Loader::_Fseekc(void)
{
	char t = fgetc(_pDB);
	fseek(_pDB, -1, SEEK_CUR);
	return t;
}

char DB_Loader::_Tag_Match(char roll_back)
{
	long start_pos = ftell(_pDB);
	_Filt_Empty();
	//如果不是遇到开始标签就返回匹配失败
	if (fgetc(_pDB) != '<')
	{
		fseek(_pDB, start_pos, SEEK_SET);
		return 0;
	}
	const static char author[] = { 'a','u','t','h','o','r' };
	const static char title[] = { 't','i','t','l','e' };
	const static char year[] = { 'y','e','a','r' };
	char match_success = 1;
	for (char i = 0; i < sizeof(author); i++)
	{
		if (fgetc(_pDB) != author[i])
		{
			match_success = 0;
			break;
		}
	}
	if (match_success)
	{
		if (fgetc(_pDB) == '>')
		{
			if (roll_back)
				fseek(_pDB, start_pos, SEEK_SET);
			return 1;
		}
	}

	//匹配title
	match_success = 1;
	for (char i = 0; i < sizeof(title); i++)
	{
		if (fgetc(_pDB) != title[i])
		{
			match_success = 0;
			break;
		}
	}
	if (match_success)
	{
		if (fgetc(_pDB) == '>')
		{
			if(roll_back)
				fseek(_pDB, start_pos, SEEK_SET);
			return 2;
		}
	}

	//匹配year
	match_success = 1;
	for (char i = 0; i < sizeof(year); i++)
	{
		if (fgetc(_pDB) != year[i])
		{
			match_success = 0;
			break;
		}
	}
	if (match_success)
	{
		if (fgetc(_pDB) == '>')
		{
			if(roll_back)
				fseek(_pDB, start_pos, SEEK_SET);
			return 3;
		}
	}

	fseek(_pDB, start_pos, SEEK_SET);
	return 0;
}

std::string DB_Loader::_Seek_Tag(char roll_back)
{
	long start_pos = ftell(_pDB);
	_Filt_Empty();
	//如果不是遇到开始标签就返回匹配失败
	if (fgetc(_pDB) != '<')
	{
		fseek(_pDB, start_pos, SEEK_SET);
		return " ";
	}
	std::string ret;
	char temp = 0;
	while (temp = fgetc(_pDB))
	{
		if((temp == '>')||(temp == ' '))
			break;
		ret += temp;
	}
	switch(roll_back)
	{
	case 1:
		if (ret.at(0) == '/')
			break;
	case 2:
		fseek(_pDB, start_pos, SEEK_SET);
		break;
	default:
		break;
	}
	return ret;
}