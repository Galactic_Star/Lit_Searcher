#pragma once
#ifndef _main_h
#define _main_h

//    版    本：	v0.0.1
//    更新日期：	2021-03-10
//    说    明：	本文件实现了对分析器的启动，是最顶层的接口，可以被控制台启动器或其他调用。
//*                 include本文件并使用analysis(数据源地址，输出文件夹地址)来开启分析过程

//*           未完成，持续更新中。

//库包含段开始
#include <stdio.h>
#include <ctime>
#include <string>
#include <vector>
#include <list>
#include <fcntl.h>
#include <unordered_map>
#include <vector>
#include "defines.h"
#include "config.h"
#include "Author_Statistics.h"
#include "hash 2.0.h"
#include "key_map.h"
#include "str_ID.h"
#include "Transformat.h"
//库包含段结束


//宏定义段开始
//宏定义段结束


//数据类型定义段开始
//数据类型定义段结束


//全局变量段开始
//全局变量段结束


//本函数会判定 source_path 所指向的源文件是否存在
//如果是则开启xml分析过程并输出分析结果到 output_path
//如果output_path不传入或传入空值，则传递 "defines.h"中定义的 DEFAULT_OUTPUT_PATH 至解析器
//返回值为 0 代表成功完成
//返回值为 -1 代表输入文件错误
int analysis(const char* source_path, const char* output_path = NULL);


//本函数会判定 source_path 所指向的源文件是否存在
//如果是则开启xml分析过程并输出分析结果（实体数量，author不去重）
//返回值为 0 代表成功完成
//返回值为 -1 代表输入文件错误
int pre_analysis(const char* source_path);

#endif		// !_main_h