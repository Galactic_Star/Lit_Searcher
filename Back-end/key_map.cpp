#include "key_map.h"

Key_Map::Key_Map()
{
	Reinit();
}

Key_Map::~Key_Map()
{
}

Key_Map::Key_Map(const Key_Map&old)
{
	*this = old;
}

Key_Map& Key_Map::operator= (const Key_Map&old)
{
	_good = old._good;
	_maps.clear();
	_maps = old._maps;
	return *this;
}



void Key_Map::Reinit()
{
	_maps.clear();
	_good = 1;
}


return_TypeDef Key_Map::Add_ID(kmHash_TypeDef hash, kmID_TypeDef id)
{
	//状态不对报错退出
	if (!_good)
		return return_TypeDef::state_error;
	//判断空表
	if (_maps.size())
	{
		//看看是否已存在相同hash
		std::unordered_map<kmHash_TypeDef, std::vector<kmID_TypeDef>>::iterator finded= _maps.find(hash);
		if (finded != _maps.end())
		{
			//可以判定存在hash，检查是否存在对应id
			int idlen = finded->second.size(),
				sameid = 0;
			for (int i = 0; i < idlen; ++i)
				if ((finded->second)[i] == id)
				{
					sameid = 1;
					break;
				}
			//确认id相同返回
			if (sameid)
				return return_TypeDef::normal;
			//同hash不同id
			else
			{
				finded->second.push_back(id);
				return return_TypeDef::normal;
			}
		}
	}
	//空表和没匹配到hash都到这里
	std::vector<kmID_TypeDef> temp;
	temp.push_back(id);
	_maps.insert(std::pair<kmHash_TypeDef, std::vector<kmID_TypeDef>>(hash, temp));

	return return_TypeDef::normal;
}

int Key_Map::Ask_ID_size(kmHash_TypeDef code)
{
	//状态检查
	if (!_good)
		return 0;
	//空表肯定0
	if (_maps.empty())
		return 0;
	//查找
	std::unordered_map<kmHash_TypeDef, std::vector<kmID_TypeDef>>::iterator finded = _maps.find(code);
	if (finded != _maps.end())
		return finded->second.size();
	else
		return 0;
}

int Key_Map::Get_ID(kmHash_TypeDef code, std::vector<kmID_TypeDef>& sto)
{
	sto.clear();

	//状态检查
	if (!_good)
		return 0;
	//空表肯定0
	if (_maps.empty())
		return 0;
	//查找
	std::unordered_map<kmHash_TypeDef, std::vector<kmID_TypeDef>>::iterator finded = _maps.find(code);
	if (finded != _maps.end())
	{
		for (std::vector<kmID_TypeDef>::iterator it = finded->second.begin(); it != finded->second.end(); it++)
			sto.push_back(*it);
		return sto.size();
	}
	else
		return 0;
	return 0;
}

//检查导出参数是否错误，是则返回1
//如果路径末尾不是 / 或 \ 就补上 \ 。
int Key_Map::wrong_para(
	std::string& path,
	const std::string& filename,
	const std::string& ext_name)
{
	//空路径认为是指向当前运行路径
	if (!path.empty())
	{
		//非空路径检查结尾
		char i = path[path.size() - 1];

		//检查及修正路径结尾
		if ((i != '/') && (i != '\\'))
		{
			//根据路径格式选择添加啥
			if (path.find('/') != std::string::npos)
				path += '/';
			else
				path += '\\';
		}
	}

	//如果两个都空则为错
	if (filename.empty() && ext_name.empty())
		return 1;
	else
		return 0;
}

//解析结果为object，均为obcect型value
// 每一value属性名为hash值（与数值型相同）
// 若仅有一个id，属性值为数值
// value值出现是无序的
return_TypeDef Key_Map::Export_JsonResult(std::string path, std::string filename, std::string ext_name, int line)
{
	//检查状态
	if (!_good)
		return return_TypeDef::state_error;
	if (_maps.empty())
		return return_TypeDef::str_id_no_output;

	//检查参数
	if (wrong_para(path, filename, ext_name))
		return return_TypeDef::path_error;


	FILE* target = NULL;
	//打开文件出错
	if (fopen_s(&target, (path + filename + (ext_name.size() ? ("." + ext_name) : "")).c_str(), "wb") != 0)
		return return_TypeDef::path_error;


	//定位到文件头
	fseek(target, 0, SEEK_SET);
	fprintf_s(target, "{");
	//根据是否换行输出正文
	if (line)
	{
		//遍历hash
		int count = _maps.size();
		std::unordered_map<kmHash_TypeDef, std::vector<kmID_TypeDef>>::iterator it = _maps.begin();
		for (;count>1; it++,--count)
		{
			fprintf_s(target, "\"%d\":",it->first);
			//多个ID时以数组形式输出
			if (it->second.size() > 1)
			{
				fprintf_s(target, "[");
				for (int i = it->second.size() - 1; i >= 1; --i)
					fprintf_s(target, "%d,", it->second[i]);
				fprintf_s(target, "%d],\n", it->second[0]);
			}
			else
				fprintf_s(target, "%d,\n", it->second[0]);
		}
		//输出最后一行
		{
			fprintf_s(target, "\"%d\":",it->first);
			//多个ID时以数组形式输出
			if (it->second.size() > 1)
			{
				fprintf_s(target, "[");
				for (int i = it->second.size() - 1; i >= 1; --i)
					fprintf_s(target, "%d,", it->second[i]);
				fprintf_s(target, "%d]}", it->second[0]);
			}
			else
				fprintf_s(target, "%d}", it->second[0]);
		}
	}
	else
	{
		//遍历hash
		int count = _maps.size(); 
		std::unordered_map<kmHash_TypeDef, std::vector<kmID_TypeDef>>::iterator it = _maps.begin();
		for (;count > 1; it++, --count)
		{
			fprintf_s(target, "\"%d\":", it->first);
			//多个ID时以数组形式输出
			if (it->second.size() > 1)
			{
				fprintf_s(target, "[");
				for (int i = it->second.size() - 1; i >= 1; --i)
					fprintf_s(target, "%d,", it->second[i]);
				fprintf_s(target, "%d],", it->second[0]);
			}
			else
				fprintf_s(target, "%d,", it->second[0]);
		}
		//输出最后一行
		{
			fprintf_s(target, "\"%d\":", it->first);
			//多个ID时以数组形式输出
			if (it->second.size() > 1)
			{
				fprintf_s(target, "[");
				for (int i = it->second.size() - 1; i >= 1; --i)
					fprintf_s(target, "%d,", it->second[i]);
				fprintf_s(target, "%d]}", it->second[0]);
			}
			else
				fprintf_s(target, "%d}", it->second[0]);
		}
	}
	//关闭文件
	fclose(target);
	target = NULL;
	return return_TypeDef::normal;
}