//库包含段开始
#include "Author_Statistics.h"	
#include <list>
//库包含段结束

//文件内宏定义段开始
//文件内宏定义段结束


//文件内全局变量段开始
//文件内全局变量段结束




Author_Statistician::Author_Statistician()
{
	Reinit();
}

Author_Statistician::Author_Statistician(Author_Statistician& old)
{
	*this = old;
}

Author_Statistician::~Author_Statistician()
{
}

Author_Statistician& Author_Statistician::operator=(Author_Statistician&old)
{
	_good = old._good;
	_authors.clear();
	_authors = old._authors;
	_max_id = old._max_id;
	return *this;
}

void Author_Statistician::Reinit()
{
	_authors.clear();
	_good = 1;
	_max_id = STR_ID_INIT_NUM;
}

return_TypeDef Author_Statistician::Add_Record(std::vector<Str_ID_TypeDef>authors, Str_ID_TypeDef article_id)
{
	//状态检查
	if (!_good)
		return return_TypeDef::state_error;
	//空参检查
	if (authors.empty())
		return return_TypeDef::normal;

	//作者检查，如果无作者则创建并添加，有作者则添加其他作者信息
	for (int i = authors.size() - 1; i >= 0; --i)
	{
		//找
		map_typedef::iterator it = _authors.find(authors[i]);
		//找不到
		if (it == _authors.end())
		{
			//比较ID大小
			if (authors[i] > _max_id)_max_id = authors[i];
			//创建信息
			_Author_TypeDef temp;
			//存放合作者信息
			for (int j = authors.size() - 1; j >= 0; --j)
			{
				if (authors[i] == authors[j])
					continue;
				else
					insert(temp.cooperations,authors[j]);
			}
			//存放文献ID信息
			insert(temp.articles,article_id);
			//存入
			_authors.insert(std::pair<Str_ID_TypeDef, _Author_TypeDef>(authors[i], temp));
		}
		//找得到
		else
		{
			//存放合作者信息
			for (int j = authors.size() - 1; j >= 0; --j)
			{
				if (authors[i] == authors[j])
					continue;
				else
					insert(it->second.cooperations,authors[j]);
			}
			//存放文献ID信息
			insert(it->second.articles,article_id);
		}
	}
	return return_TypeDef::normal;
}

int Author_Statistician::Ask_articleNum(const Str_ID_TypeDef id)
{
	//没记录或状态机不对肯定0
	if (_authors.empty())
		return 0;
	if (!_good)
		return 0;
	map_typedef::iterator res = _authors.find(id);
	//找不到返回0
	if (res == _authors.end())
		return 0;
	//找得到
	return res->second.articles.size();
}


int Author_Statistician::Get_Article(Str_ID_TypeDef id, std::vector<Str_ID_TypeDef>& sto)
{
	sto.clear();
	sto.shrink_to_fit();
	//没记录或状态机不对肯定0
	if (_authors.empty())
		return 0;
	if (!_good)
		return 0;
	map_typedef::iterator res = _authors.find(id);
	//找不到返回0
	if (res == _authors.end())
		return 0;
	//找得到
	sto = res->second.articles;
	return sto.size();
}

int Author_Statistician::Ask_cooperatorNum(const Str_ID_TypeDef id)
{
	//没记录或状态机不对肯定0
	if (_authors.empty())
		return 0;
	if (!_good)
		return 0;
	map_typedef::iterator res = _authors.find(id);
	//找不到返回0
	if (res == _authors.end())
		return 0;
	//找得到
	return res->second.cooperations.size();
}

int Author_Statistician::Get_Cooperator(Str_ID_TypeDef id, std::vector<Str_ID_TypeDef>& sto)
{
	sto.clear();
	sto.shrink_to_fit();
	//没记录或状态机不对肯定0
	if (_authors.empty())
		return 0;
	if (!_good)
		return 0;
	map_typedef::iterator res = _authors.find(id);
	//找不到返回0
	if (res == _authors.end())
		return 0;
	//找得到
	sto = res->second.cooperations;
	return sto.size();
}


//检查导出参数是否错误，是则返回1
//如果路径末尾不是 / 或 \ 就补上 \ 。
int Author_Statistician::wrong_para(
	std::string& path,
	const std::string& filename,
	const std::string& ext_name,
	const int& pieces)
{
	//空路径认为是指向当前运行路径
	if (!path.empty())
	{
		//非空路径检查结尾
		char i = path[path.size() - 1];

		//检查及修正路径结尾
		if ((i != '/') && (i != '\\'))
		{
			//根据路径格式选择添加啥
			if (path.find('/') != std::string::npos)
				path += '/';
			else
				path += '\\';
		}
	}

	//分片，无需检查前后缀
	if (pieces)
		return 0;
	//如果两个都空则为错
	if (filename.empty() && ext_name.empty())
		return 1;
	else
		return 0;
}

//不含任何状态检查因此需确保正确
void Author_Statistician::write_max_au(FILE* target, int line) {
	//比较处理使用的结构体
	struct au_t
	{
		Str_ID_TypeDef id;
		unsigned int count;
	};
	//开始产生结果
	std::vector<au_t> max_au;
	max_au.reserve(AUTHOR_MAX_ATRICLE_NUM + 1);
	for (map_typedef::iterator it = _authors.begin(); it != _authors.end(); it++)
	{
		//排序插入
		{
			au_t value{ it->first, it->second.articles.size() };
			if (max_au.empty())
			{
				max_au.push_back(value);
				continue;
			}
			char unfounded = 1;
			std::vector<au_t>::iterator i = max_au.begin();
			//大于就向后推，可以造降序序列
			for (; i != max_au.end(); ++i)
			{
				if (i->count <= value.count)
				{
					max_au.insert(i, value);
					break;
				}
			}
			//说明到队尾还是处于小于
			if(max_au.size()< AUTHOR_MAX_ATRICLE_NUM)
				max_au.push_back(value);
		}
		if (max_au.size() > AUTHOR_MAX_ATRICLE_NUM)
			max_au.pop_back();
	}
	//结果已产出
	//写入最大id
	std::vector<au_t>::iterator it = max_au.begin();
	fprintf_s(target, "\"maxid\":[%d", it->id);
	++it;
	if (line)
		for (; it != max_au.end(); ++it)
			fprintf_s(target, ",\n%d", it->id);
	else
		for (; it != max_au.end(); ++it)
			fprintf_s(target, ",%d", it->id);
	//写入最大数量
	it = max_au.begin();
	if (line)
		fprintf_s(target, "],\n\"maxnum\":[%d", it->count);
	else
		fprintf_s(target, "],\"maxnum\":[%d", it->count);
	++it;
	if (line)
		for (; it != max_au.end(); ++it)
			fprintf_s(target, ",\n%d", it->count);
	else
		for (; it != max_au.end(); ++it)
			fprintf_s(target, ",%d", it->count);
	fprintf_s(target, "]");

	if (line)
		fprintf_s(target, "\n");
	max_au.clear();
}

//不分片下解析结果为object，包括：
//  数值型offset，为 STR_ID_INIT_NUM 的值
//	数组型maxid（文献最多的 AUTHOR_MAX_ATRICLE_NUM 人的ID，数组内均为数值型）
//	数组型maxnum（文献最多的 AUTHOR_MAX_ATRICLE_NUM 人的文献数，数组内均为数值型），下标与maxid对应
//	数组型art，其中每个个体为数组型文献ID（即使只有一个），其下标对应 作者ID - STR_ID_INIT_NUM
//	数组型cop，其中每个个体为	数组型，合作者ID列表（即使只有一个合作者），其下标对应 作者ID - STR_ID_INIT_NUM
//  如果id下为空使用 null 占位
//分片时
//	在 <filename>info.<ext_name>下生成数值型的offset（初始id）、num（文件总数）、
//		size（条目总数）、length（文件序号长度）、pieces、
//		maxid（文献数前 AUTHOR_MAX_ATRICLE_NUM 个作者id，降序排列，数值数组，不分片）
//		maxnum（文献最多的 AUTHOR_MAX_ATRICLE_NUM 人的文献数，数组内均为数值型），下标与maxid对应
//	在 <filename><k>.<ext_name> 中为对象，包括两个数组
//		每个数组存放 ID 从 offset + k* pieces 至 offset + (k+1)* pieces -1 的记录
//		两个数组分别名为art与cop
//		k从全0开始，以十六进制表示
return_TypeDef Author_Statistician::Export_JsonResult(std::string path, std::string filename, std::string ext_name, int pieces, int line)
{

	//检查状态
	if (!_good)
		return return_TypeDef::state_error;
	if (_authors.empty())
		return return_TypeDef::str_id_no_output;

	//检查参数
	if (wrong_para(path, filename, ext_name, pieces))
		return return_TypeDef::path_error;


	FILE* target = NULL;
	//不分片逻辑
	if (!pieces)
	{
		//打开文件出错
		if (fopen_s(&target, (path + filename + (ext_name.size() ? ("." + ext_name) : "")).c_str(), "wb") != 0)
			return return_TypeDef::path_error;

		//定位到文件头
		fseek(target, 0, SEEK_SET);
		fprintf_s(target, "{\"offset\":%d,", STR_ID_INIT_NUM);

		if (line)
			fprintf_s(target, "\n");

		//最大的 AUTHOR_MAX_ATRICLE_NUM 个id的处理
		write_max_au(target, line);
		fprintf_s(target, ",");
		if (line)
			fprintf_s(target, "\n");

		//处理art表
		{
			fprintf_s(target, "\"art\":[");
			//记录已导出个数，用于判定是否完成导出
			int output_count = _authors.size();
			map_typedef::iterator it;
			for (Str_ID_TypeDef id = STR_ID_INIT_NUM; output_count > 0 ; ++id)
			{
				it = _authors.find(id);
				if (it != _authors.end())
				{
					--output_count;
					std::vector<Str_ID_TypeDef>::iterator it2 = it->second.articles.begin();
					fprintf_s(target, "[%d",*it2);
					++it2;
					for (; it2 != it->second.articles.end(); ++it2)
						fprintf_s(target, ",%d", *it2);
					fprintf_s(target, "]");
				}
				else
					fprintf_s(target, "null");
				if(output_count == 0)
					fprintf_s(target, "],");
				else
					fprintf_s(target, ",");
				if(line)
					fprintf_s(target, "\n");
			}
		}

		//处理cop表
		{
			fprintf_s(target, "\"cop\":[");
			//记录已导出个数，用于判定是否完成导出
			int output_count = _authors.size();
			map_typedef::iterator it;
			for (Str_ID_TypeDef id = STR_ID_INIT_NUM; output_count > 0; ++id)
			{
				it = _authors.find(id);
				if (it != _authors.end())
				{
					--output_count;
					std::vector<Str_ID_TypeDef>::iterator it2 = it->second.cooperations.begin();
					fprintf_s(target, "[%d", *it2);
					++it2;
					for (; it2 != it->second.cooperations.end(); ++it2)
						fprintf_s(target, ",%d", *it2);
					fprintf_s(target, "]");
				}
				else
					fprintf_s(target, "null");
				if (output_count == 0)
					fprintf_s(target, "]");
				else
					fprintf_s(target, ",");
				if (line)
					fprintf_s(target, "\n");
			}
		}
		//输出右侧括号
		fprintf_s(target, "}");
		//关闭文件
		fclose(target);
		target = NULL;
		return return_TypeDef::normal;
	}
	else
	{
		//分片逻辑

		//用于存放文件名逻辑的字符串
		char* pathexpr = NULL;
		int pathexpr_size = 0;
		//存放标号长度
		int length = 0;

		//准备写入config
		{
			//打开文件出错
			if (fopen_s(&target, (path + filename + (ext_name.size() ? ("info." + ext_name) : "info")).c_str(), "wb") != 0)
				return return_TypeDef::path_error;
			//定位到文件头
			fseek(target, 0, SEEK_SET);
			int size = _max_id - STR_ID_INIT_NUM + 1,
				num = 0;
			//计算文件数
			num = size / pieces;
			if (size % pieces)
				++num;
			//计算序号长度
			for (int i = num; i; (i >>= 4), ++length);
			//写入 config 信息
			fprintf_s(target,
				"{\"offset\":%d,\n\"num\":%d,\n\"size\":%d,\n\"length\":%d,\n\"pieces\":%d,\n",
				STR_ID_INIT_NUM, num, size, length, pieces);
			write_max_au(target, line);
			fprintf_s(target, "}");
			//关闭 info
			fclose(target);
			target = NULL;

			//写入 pathexpr 信息
			// 
			//tempc为确定文件名长度的字符串
			char tempc[10] = { 0 };
			sprintf_s(tempc, "%%0%dX", length);
			//temps为文件名格式化字符串
			std::string temps = path + filename + tempc;
			if (ext_name.size())
				temps += "." + ext_name;
			pathexpr_size = temps.size() + 3;
			pathexpr = new char[pathexpr_size];
			//如果申请失败报错
			if (pathexpr == NULL)
				return return_TypeDef::unexpected_error;
			//拷贝表达式
			strcpy_s(pathexpr, pathexpr_size, temps.c_str());
		}

		//主输出功能
		//if(0)
		{
			//存放正式文件名的空间
			char* cur_target_p = NULL;
			int  cur_target_sz = pathexpr_size + length + 3;

			cur_target_p = new char[cur_target_sz];
			//如果申请失败报错
			if (cur_target_p == NULL)
				return return_TypeDef::unexpected_error;


			//循环输出主程序

			//剩余需要输出的作者数量（包括未被记录的 null）
			int to_out = _max_id - STR_ID_INIT_NUM +1;
			for (int filecnt/* 文件编号 */= 0, cur_id/* 当前正在读写的ID */= STR_ID_INIT_NUM;
				to_out > 0; ++filecnt, to_out -= pieces)
			{
				//生成当前文件名
				sprintf_s(cur_target_p, cur_target_sz, pathexpr, filecnt);

				FILE* cur_target = NULL;
				//打开文件出错
				if (fopen_s(&cur_target, cur_target_p, "wb") != 0)
					return return_TypeDef::path_error;
				//定位到文件头
				fseek(cur_target, 0, SEEK_SET);
				//备份id
				Str_ID_TypeDef id_bak = cur_id;
				//处理art
				{
					fprintf_s(cur_target, "{\"art\":[");
					//生成循环结束条件
					int stop = (to_out > pieces ? pieces : to_out) - 1;
					//根据换行条件循环输出id的前n字节
					map_typedef::iterator it;
					for (int i = stop; i >= 0; --i, ++cur_id)
					{
						it = _authors.find(cur_id);
						if ((it != _authors.end())&&(it->second.articles.size()))
						{
							std::vector<Str_ID_TypeDef>::iterator it2 = it->second.articles.begin();
							fprintf_s(cur_target, "[%d", *it2);
							++it2;
							for (; it2 != it->second.articles.end(); ++it2)
								fprintf_s(cur_target, ",%d", *it2);
							fprintf_s(cur_target, "]");
						}
						else
							fprintf_s(cur_target, "null");
						if (i == 0)
							fprintf_s(cur_target, "],");
						else
							fprintf_s(cur_target, ",");
						if (line)
							fprintf_s(cur_target, "\n");
					}
				}
				cur_id = id_bak;
				//处理cop
				{
					fprintf_s(cur_target, "\"cop\":[");
					//生成循环结束条件
					int stop = (to_out > pieces ? pieces : to_out) - 1;
					//根据换行条件循环输出id的前n字节
					map_typedef::iterator it;
					for (int i = stop; i >= 0; --i, ++cur_id)
					{
						it = _authors.find(cur_id);
						if ((it != _authors.end())&&(it->second.cooperations.size()))
						{
							std::vector<Str_ID_TypeDef>::iterator it2 = it->second.cooperations.begin();
							fprintf_s(cur_target, "[%d", *it2);
							++it2;
							for (; it2 != it->second.cooperations.end(); ++it2)
								fprintf_s(cur_target, ",%d", *it2);
							fprintf_s(cur_target, "]");
						}
						else
							fprintf_s(cur_target, "null");
						if (i == 0)
							fprintf_s(cur_target, "]}");
						else
							fprintf_s(cur_target, ",");
						if (line)
							fprintf_s(cur_target, "\n");
					}
				}
				//关闭文件
				fclose(cur_target);
			}

			//释放文件名空间
			delete[] cur_target_p;
		}

		//释放表达式占用的内存
		delete[] pathexpr;
	}
	return return_TypeDef::normal;
}


void Author_Statistician::insert(std::vector<Str_ID_TypeDef>& target, const Str_ID_TypeDef& value)
{
	if (target.empty())
	{
		target.push_back(value);
		return;
	}
	char unfounded = 1; 
	std::vector<Str_ID_TypeDef>::iterator i = target.begin();
	//小于就向后推，可以造升序序列
	for (; i != target.end(); ++i)
	{
		if (*i == value)
			return;
		else if (*i > value)
		{
			target.insert(i,value);
			return;
		}

	}
	//说明到队尾还是处于小于
	target.push_back(value);
}