#pragma once
#ifndef _hash_h
#define _hash_h

//    ��    ����	v1.0.0
//    �������ڣ�	2021-05-09
//    ˵    ����	���ļ�ʵ����������ϣ����������ָ�����ȵĹ�ϣֵ��
//					����ҳ�˵��õ� hash.js ����ͬ����
//					�������㷨�� F ʵ�֣��˺��� G ����ͷ�ļ���


//������ο�ʼ
#include<iostream>
#include<string>
//������ν���


//�궨��ο�ʼ
//�궨��ν���


//�������Ͷ���ο�ʼ
//�������Ͷ���ν���


//ȫ�ֱ����ο�ʼ
//ȫ�ֱ����ν���


// BKDR Hash Function
unsigned int BKDRHash(std::string str);

//���� 8bit ���ȵĹ�ϣֵ
unsigned int BKDRHash8(std::string str);

//���� 12bit ���ȵĹ�ϣֵ
unsigned int BKDRHash12(std::string str);

//���� 16bit ���ȵĹ�ϣֵ
unsigned int BKDRHash16(std::string str);

//���� 32bit ���ȵĹ�ϣֵ
unsigned int BKDRHash32(std::string str);


#endif		// !_hash_h