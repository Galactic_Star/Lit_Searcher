#include "hash.h"

// BKDR Hash Function
unsigned int BKDRHash(std::string str)
{
    unsigned int seed = 131; 
    unsigned int hash = 0;
    int pos = 0;
    int len = str.length();
    while (pos < len)
    {
        hash = hash * seed + str[pos];
        pos++;
    }

    return (hash & 0x7FFFFFFF);
}

unsigned int BKDRHash8(std::string str)
{
    unsigned int seed = 131; 
    unsigned int hash = 0;
    int pos = 0;
    int len = str.length();
    while (pos < len)
    {
        hash = hash * seed + str[pos];
        pos++;
    }

    return (hash & 0xFF);
}

unsigned int BKDRHash12(std::string str)
{
    unsigned int seed = 131; 
    unsigned int hash = 0;
    int pos = 0;
    int len = str.length();
    while (pos < len)
    {
        hash = hash * seed + str[pos];
        pos++;
    }

    return (hash & 0xFFF);
}

unsigned int BKDRHash16(std::string str)
{
    unsigned int seed = 131; 
    unsigned int hash = 0;
    int pos = 0;
    int len = str.length();
    while (pos < len)
    {
        hash = hash * seed + str[pos];
        pos++;
    }

    return (hash & 0xFFFF);
}

unsigned int BKDRHash32(std::string str)
{
    unsigned int seed = 131; 
    unsigned int hash = 0;
    int pos = 0;
    int len = str.length();
    while (pos < len)
    {
        hash = hash * seed + str[pos];
        pos++;
    }

    return (hash & 0xFFFFFFFF);
}
