#ifndef _B_tree_h
#define _B_tree_h

//    版    本：	v0.1
//    更新日期：	2021-03-09
//    说    明：	本文件实现了B树的模板类，本B树使用了运算符 < 对两个数据进行比较，因此如果使
// *                用的是自定义结构（如自定义类或管理指针类型），记得重载这一运算符。
// *                此外，如果想要对承载自定义类型数据的树使用 == 比较，记得重载 == 运算符。
// *
// *  类    名：    b_tree
// *  参    数：    Data，荷载的数据类型
// *  运算符支持：  =  赋值，旧树的所有内容会被释放
// *                == 比较。如果两棵树完全相同才返回真。
// *                
// *                
// *                
// *            还在设计阶段，以上所有描述均可能改变
// *


//库包含段开始
//库包含段结束


//宏定义段开始
//宏定义段结束


//数据类型定义段开始
//数据类型定义段结束


//全局变量段开始
//全局变量段结束











#endif		// !_B_tree_h