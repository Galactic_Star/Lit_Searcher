#pragma once
#ifndef _defines_h
#define _defines_h

//    版    本：	v0.0.1
//    更新日期：	2021-03-09
//    说    明：	本文件提供对各种全局使用的数据类型的定义


//库包含段开始
//库包含段结束


//宏定义段开始

//在不指定输出位置下的默认输出位置
#define DEFAULT_OUTPUT_PATH "/analysis_result/"


//宏定义段结束


//数据类型定义段开始

//定义全局使用的 int 型返回值语义
enum class return_TypeDef: int
{
	normal = 0,					//正常退出
	input_path_error,			//输入路径错误
	not_a_dblp_db,				//认为不是正确的dblp数据库

	input_ptr_error,			//断点恢复时传入的指针位置错误（暂未使用）

	path_error,					//通用路径错误
	state_error,				//通用状态机错误
	unexpected_error,			//通用未定义的错误

	xml_tag_name_error,			//XML 分析时发现非法标签名
	xml_name_not_found,			//查找子对象时没有符合名称的对象

	str_id_no_output,			//没有可以导出的字符串 - ID关系（列表为空）

	function_not_realized,		//函数未实现
};
//数据类型定义段结束


//全局变量段开始

//全局变量段结束











#endif		// !_defines_h