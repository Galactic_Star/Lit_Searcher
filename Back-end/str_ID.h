#pragma once
#ifndef _str_ID_h
#define _str_ID_h

//    版    本：	v0.1.1
//    更新日期：	2021-05-13
//    说    明：	本文件实现了由string到ID的自动操作（编码自动创建、编码匹配、关联关系的导出）
//					通过类 Str_ID_Mgr 的对象来访问。
//					使用 Get_Str_ID (std::string name) 方法可以自动查找ID（不存在则自动创建）
//					使用 Export_BinResult(...)  方法可以导出二进制结果至指定路径（未实现）
//					使用 Export_JsonResult(...)  方法可以导出JSON格式结果至指定路径
//					使用 Load(...) 方法可以加载二进制格式的结果（未实现）
//


//库包含段开始
#include <string>
#include <fcntl.h>
#include <unordered_map>
#include <vector>
#include "config.h"
#include "defines.h"
//库包含段结束


//宏定义段开始
//宏定义段结束


//数据类型定义段开始
//数据类型定义段结束


//全局变量段开始
//全局变量段结束


//由字符串内容到unsigned int 型的
class Str_ID_Mgr
{
public:
	Str_ID_Mgr();
	Str_ID_Mgr(Str_ID_Mgr&);
	~Str_ID_Mgr();

	Str_ID_Mgr& operator=(Str_ID_Mgr&);

	//重置
	void Reinit(void);

	//根据传入的字符串返回对应的ID
	//如果字符串原先不存在于表中，则插入并返回新ID
	//如果插入失败则返回 STR_ID_FAILED_NUM
	Str_ID_TypeDef Get_Str_ID(const std::string& name);

	//根据传入的字符串查找对应的ID，写入result_ID 中
	//如果插入失败则写入 STR_ID_FAILED_NUM
	//如果字符串原先不存在于表中，则插入，插入成功返回零，失败返回负值，已存在则返回正值
	char Get_Str_ID(const std::string& name, Str_ID_TypeDef& result_ID);

	//根据传入的字符串查找是否已记录了对应的ID
	//如果字符串原先不存在于表中，则返回零，否则返回正值
	//状态错误则返回负值
	char Exist_Str(const std::string& name)const;

	unsigned int size()const;

	//参数说明：
	//	path	 ： 导出文件（组）的路径（文件夹名称）， 可由 \ / 结束或不带（自动添加）
	// 	filename ： 文件名称，如果启用分片则为分片前缀名称（可以为空）
	//	ext_name ： 扩展名名称（不带 . ）。不启用分片时与filename不可同时为空
	// 	pieces	 ： 分片大小。为0时关闭分片，否则为每一分片大小
	//				（如果大于当前ID最大值依旧以分片模式创建）
	// 	clear（未使用）    ： 输出后清空本对象（相当于重新初始化），非零时清空
	// 
	//返回值：
	//	正常创建时返回 normal 
	// 	如果没有可以导出的关系对（列表为空）返回 str_id_no_output
	// 	路径错误 返回 path_error
	//	状态错误 返回 state_error
	//	其余未定义错误 返回 unexpected_error
	//	该函数未实现
	return_TypeDef Export_BinResult(std::string path, std::string filename, std::string ext_name, int pieces = 0);

	//向目标位置写出一个（一组）JSON文件，如有行尾则为 \n ，文件包括两端 [] （导出为数组）
	//参数说明：
	//	path	 ： 导出文件（组）的路径（文件夹名称）， 可由 \ / 结束或不带（自动添加）
	// 	filename ： 文件名称，如果启用分片则为分片前缀名称（可以为空）
	//	ext_name ： 扩展名名称（不带 . ）。不启用分片时与filename不可同时为空
	// 	pieces	 ： 分片大小。为0时关闭分片，否则为每一分片大小（如果大于当前ID最大值依旧以分片模式创建）
	// 	line    ： 每一字符串后换行（不推荐，默认关闭）。非零时启用
	// 	clear（未使用）    ： 输出后清空本对象（相当于重新初始化），非零时清空
	// 
	//返回值：
	//	正常创建时返回 normal 
	// 	如果没有可以导出的关系对（列表为空）返回 str_id_no_output
	// 	路径错误 返回 path_error
	//	状态错误 返回 state_error
	//	其余未定义错误 返回 unexpected_error
	return_TypeDef Export_JsonResult(std::string path, std::string filename, std::string ext_name, int pieces = 0,int line = 0);

	//加载二进制结果文件
	//参数说明：
	// 	file_path	：文件路径（含文件名）
	// 	clear		：是否清空已有列表
	//返回值：
	//	成功读取返回 字符串 - ID 对数量
	//	如果错误返回 -1
	//	该函数未实现
	int Load(const std::string& file_path);

private:
	std::unordered_map<std::string, Str_ID_TypeDef> _str_ids;
	Str_ID_TypeDef _next_avaliable;
	bool _good;
	Str_ID_TypeDef _init_id;


	int wrong_para(
		std::string& path,
		const std::string& filename,
		const std::string& ext_name,
		const int& pieces);

};


#endif		// !_template_h