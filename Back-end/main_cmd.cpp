//    版    本：	v0.2.0
//    更新日期：	2021-05-13
//    说    明：	本文件为控制台程序的入口，如果使用图形化入口进行编译则应关闭本文件的第一个define
#define _main_cpp
#ifdef _main_cpp

#include <stdio.h>
#include <iostream>
#include <conio.h>
#include "main.h"
#include "defines.h"

//测试模式切换（用于单模块调试与主控的切换）
//#define _TEST_MODE
#ifndef _TEST_MODE

//参数列表内容：
// 第一个参数（必选）为输入dblp数据库的xml格式文件
// 第二个参数（可选）为输出的文件组的地址，如不指定的默认值由defines.h 文件中 DEFAULT_OUTPUT_PATH 指定
//           该默认值指定由 "main.cpp" 中的 analysis 函数实现
int main(int argc, char* argv[]) {

	int result = 0;	//分析程序的结果

	if (argc < 2)			//参数列表长为1，说明没有传入源文件地址，进入交互模式
	{
		printf("没有输入数据文件的路径，进入交互模式\n");
		printf("You didn't input the path of source document. Enter interactive mode.\n");
		printf("请输入dblp.xml文件位置（包含其文件名），按回车继续。\n");
		printf("文件如出现中文，应当使用ANSI编码：\n");
		printf("If containing Chinese, ANSI should be used ...\n\n");
		char path[INTERACTIVE_FILE_PATH_MAX_LENGTH] = { 0 };
		std::cin.getline(path, INTERACTIVE_FILE_PATH_MAX_LENGTH);
		printf("\n请输入输出文件夹位置，按回车开始分析过程。\n\n");
		char opath[INTERACTIVE_FILE_PATH_MAX_LENGTH] = { 0 };
		std::cin.getline(opath, INTERACTIVE_FILE_PATH_MAX_LENGTH);
		result = analysis(path,opath);
	}
	else
	{
		printf("正在启动分析过程，文件如出现中文，应当使用ANSI编码...\n");
		printf("System initializing.\n    if containing Chinese, ANSI should be used ...\n");
		if (argc == 2)			//有两个参数列表，说明只传入源路径
			result = analysis(argv[1]);
		else					//输入两个以上的参数只取两个
			result = analysis(argv[1], argv[2]);
	}

	//对分析程序的返回值进行分析并告知用户
	switch (result)
	{
	case 0:			//已知的成功返回值
	{
		printf("分析完成，正在准备退出！\n");
		printf("Analysising finished. Going to exit...\n");
		break;
	}
	case -1:			//已知的路径出错返回值
	{
		printf("输入路径有误，请检查！\n");
		printf("Input path error. Please check and try later.\n");
		break;
	}
	default:						//通用的错误告知
		printf("遇到了未知错误，错误代码为0x%X\n", result);
		printf("Something bad happened. Error Code is : 0x%X", result);
		break;
	}
	system("pause");
	return 0;
}

#else

//#define __TEST_STR_ID__
#ifdef __TEST_STR_ID__

#include "str_ID.h"
int main()
{
	Str_ID_Mgr s0;

	for (int i = 0; i < 2; ++i)
	{
		for (char j = 'A'; j < 'H'; ++j)
		{
			for (char k = 'a'; k < 'h'; ++k)
			{
				char temp[3] = { j,k,0 };
				printf("%s ID %d  \t", temp, s0.Get_Str_ID(temp));
			}
			printf("\n");
		}
		printf("\n");
	}
	s0.Export_JsonResult("E:\\ProgramData\\VS_Project\\Lit_Searcher\\Debug", "test", "txt", 0, 1);

	return 0;
}
#endif // __TEST_STR_ID__


//#define __TEST_KEY_MAP__
#ifdef __TEST_KEY_MAP__

#include "key_map.h"
int main()
{
	Key_Map k0;

	for (int i = 0; i < 2; ++i)
	{
		for (int j = 1; j < 1024; j <<= 1)
		{
			for (int k = 1; k <= j; k <<= 1)
			{
				printf("(%d, %d)  \t", j, k);
				k0.Add_ID(j, k);
			}
			printf("%d\n", k0.Ask_ID_size(j));
		}
		printf("\n");
	}
	k0.Export_JsonResult("E:\\ProgramData\\VS_Project\\Lit_Searcher\\Debug", "test", "txt", 1);

	return 0;
}
#endif // __TEST_KEY_MAP__


//#define __TEST_XML2JSON__
#ifdef __TEST_XML2JSON__
#include "defines.h"
#include "Transformat.h"
#include <iostream>
using std::cin;

int main()
{
	
	char s[] = "F:\\【华工】\\大二\\大二下\\数据结构大作业\\dblp.xml";
	DB_Loader db;
	db.ReLoad_DB(s, 0);
	DB_item i0;
	printf("[");
	for (int i = 0; i < 10; ++i)
	{
		i0 = db.Load_Item();
		printf("{\"type\":\"");
		printf(i0.type.c_str());
		printf("\",");
		printf(i0.json.c_str());
		if(i<9)
		printf("},\n");
		else
		printf("}]\n");
	}
	i0 = db.Load_Item();
	printf("type:\t");
	printf(i0.type.c_str());
	printf("\n\njson:\t");
	printf(i0.json.c_str());
	printf("\n\nname:\t");
	printf(i0.name.c_str());
	printf("\n\nauthors:\t");
	std::list<std::string>::iterator it = i0.authors.begin();
	for (; it != i0.authors.end(); ++it)
	{
		printf(it->c_str());
		printf("\n");
	}

	printf("year:\t%d\n", i0.year);
	return 0;
}

#endif // __TEST_XML2JSON__


//#define __TEST_ASS__
#ifdef __TEST_ASS__
#include "defines.h"
#include "config.h"
#include "Author_Statistics.h"
#include <iostream>
using std::cin;

int main()
{
	Author_Statistician a0;
	std::vector<Str_ID_TypeDef> aa;
	for (int i = 0; i < 10;i+=2)
	{
		aa.push_back( STR_ID_INIT_NUM + i);
	}
	for (int j = 0; j < 10; ++j)
	{
		a0.Add_Record(aa, j + STR_ID_INIT_NUM);
	}
	a0.Export_JsonResult("E:\\ProgramData\\VS_Project\\Lit_Searcher\\Debug", "test", "txt",0,1);
	return 0;
}

#endif // __TEST_ASS__



#define __TEST_MAIN__
#ifdef __TEST_MAIN__
#include "defines.h"
#include "config.h"
#include "main.h"
#include <iostream>
using std::cin;

int main()
{
	printf("\n\n%d",analysis("F:\\【华工】\\大二\\大二下\\数据结构大作业\\dblp.xml", "E:\\ProgramData\\VS_Project\\Lit_Searcher\\Debug\\main"));
	return 0;
}

#endif // __TEST_MAIN__


#endif // !_TEST_MODE

#endif // _main_cpp


