#include <algorithm>
#include "str_ID.h"


typedef std::unordered_map<std::string, Str_ID_TypeDef> sim_typedef;

Str_ID_Mgr::Str_ID_Mgr()
{
	Reinit();
}

Str_ID_Mgr::Str_ID_Mgr(Str_ID_Mgr&old)
{
	*this = old;
}

Str_ID_Mgr::~Str_ID_Mgr()
{

}

Str_ID_Mgr& Str_ID_Mgr::operator=(Str_ID_Mgr&old)
{
	_init_id = old._init_id;
	_good = old._good;
	_next_avaliable = old._next_avaliable;
	_str_ids = old._str_ids;
	return *this;
}

void Str_ID_Mgr::Reinit(void)
{
	_str_ids.clear();
	_next_avaliable = STR_ID_INIT_NUM;
	_good = 1;
	_init_id = STR_ID_INIT_NUM;
}

Str_ID_TypeDef Str_ID_Mgr::Get_Str_ID(const std::string& name)
{
	Str_ID_TypeDef temp = 0;
	Get_Str_ID(name, temp);
	return temp;
}

char Str_ID_Mgr::Get_Str_ID(const std::string& name, Str_ID_TypeDef& result_ID)
{
	//状态出错刹停
	if (!_good)
	{
		result_ID = STR_ID_FAILED_NUM;
		return -1;
	}
	if (_str_ids.size())
	{
		//查找元素
		sim_typedef::iterator fin = _str_ids.find(name);
		//找的到
		if (fin != _str_ids.end())
		{
			result_ID = fin->second;
			return 1;
		}
	}	
	//找不到和空列表都到这里来了
	//数量超限
	if (_next_avaliable >= STR_ID_ALLOWED_MAX_ID)
	{
		result_ID = STR_ID_FAILED_NUM;
		//上锁
		_good = 0;
		return -1;
	}
	//正常情况执行插入
	_str_ids.insert(sim_typedef::value_type(name, _next_avaliable));
	result_ID = _next_avaliable;
	++_next_avaliable;
	return 0;
}

char Str_ID_Mgr::Exist_Str(const std::string& name)const
{
	//空表
	if (_str_ids.size() == 0)
		return 0;
	//找得到
	if (_str_ids.find(name) != _str_ids.end())
		return 1;
	//其他情况：找不到
	return 0;
}

unsigned int Str_ID_Mgr::size()const
{
	return _next_avaliable - 1 - STR_ID_INIT_NUM;
}

//检查导出参数是否错误，是则返回1
//如果路径末尾不是 / 或 \ 就补上 \ 。
int Str_ID_Mgr::wrong_para(
	std::string& path,
	const std::string& filename,
	const std::string& ext_name,
	const int& pieces)
{
	//空路径认为是指向当前运行路径
	if (!path.empty())
	{
		//非空路径检查结尾
		char i = path[path.size() - 1];

		//检查及修正路径结尾
		if ((i != '/') && (i != '\\'))
		{
			//根据路径格式选择添加啥
			if (path.find('/') != std::string::npos)
				path += '/';
			else
				path += '\\';
		}
	}
	
	//分片，无需检查前后缀
	if (pieces)
		return 0;
	//如果两个都空则为错
	if (filename.empty() && ext_name.empty())
		return 1;
	else
		return 0;
}

//排序用的比较函数
bool compare_in_store(std::pair<std::string, Str_ID_TypeDef>a,
	std::pair<std::string, Str_ID_TypeDef>b)
{
	return a.second < b.second;
}

//将哈希表预排序（升序）
//此操作会清空 to
void prepare_list(sim_typedef& from,
	std::vector<std::pair<std::string, Str_ID_TypeDef>>& to)
{
	to.clear();
	to.reserve(from.size());
	for (sim_typedef::iterator it = from.begin(); it != from.end(); it++)
	{
		to.push_back(*it);
	}
	std::sort(to.begin(), to.end(), compare_in_store);
}

//二进制文件逻辑为：
// 0 - 3 Byte  ：ID组偏移量（记为A，不分片或为第一个分片时，为 STR_ID_INIT_NUM）
// 4 - 7 Byte  ：本文件内ID数量，记为 n
// 8 - (7 + 4n)：每 4Byte 为一组，表示当前片内偏移为 K（0 to n-1） 的字符串的起始位置，读至文件尾或 K+1 起始前为字符串
// 剩余字节    ：各个字符串
// 字符串ID可以表示为 A+K
return_TypeDef Str_ID_Mgr::Export_BinResult(
	std::string path,
	std::string filename,
	std::string ext_name, 
	int pieces)
{
	//检查状态
	if (!_good)
		return return_TypeDef::state_error;
	if (_str_ids.empty())
		return return_TypeDef::str_id_no_output;

	//检查参数
	if (wrong_para(path, filename, ext_name, pieces))
		return return_TypeDef::path_error;

	return return_TypeDef::function_not_realized;

	//不分片逻辑
	if (!pieces)
	{
		FILE* target = NULL;
		//打开文件出错
		if(fopen_s(&target,(path + filename + "." + ext_name).c_str(), "wb")!=0)
			return return_TypeDef::path_error;

		//排序准备输出
		std::vector<std::pair<std::string, Str_ID_TypeDef>> order;
		prepare_list(_str_ids, order);

		//输出文件头
		fseek(target, 0, SEEK_SET);

		fwrite(&_init_id, sizeof(_init_id), 1, target);
		
	}
}

//不分片下解析结果为object，包括数值型offset（初始id）和数组型value
//分片时
//	在 <filename>congig.<ext_name>下生成数值型的offset（初始id）、num（文件总数）、size（条目总数）、length（文件序号长度）、pieces
//	在 <filename><k>.<ext_name> 中为纯数组，存放 offset + k* pieces 至 offset + (k+1)* pieces -1条
//		k从全0开始，以十六进制表示
return_TypeDef Str_ID_Mgr::Export_JsonResult(
	std::string path,
	std::string filename,
	std::string ext_name,
	int pieces,
	int line)
{
	//检查状态
	if (!_good)
		return return_TypeDef::state_error;
	if (_str_ids.empty())
		return return_TypeDef::str_id_no_output;

	//检查参数
	if (wrong_para(path, filename, ext_name, pieces))
		return return_TypeDef::path_error;


	FILE* target = NULL;
	//不分片逻辑
	if (!pieces)
	{
		//打开文件出错
		if (fopen_s(&target, (path + filename + (ext_name.size()?("." + ext_name):"")).c_str(), "wb") != 0)
			return return_TypeDef::path_error;

		//排序准备输出
		std::vector<std::pair<std::string, Str_ID_TypeDef>> order;
		prepare_list(_str_ids, order);

		//定位到文件头
		fseek(target, 0, SEEK_SET);
		//fwrite("[", 1, 1, target);
		fprintf_s(target, "{\"offset\":%d,\"value\":[",_init_id);
		//根据是否换行输出正文
		if (line)
		{
			fprintf_s(target, "\n");
			int max = order.size() - 1;
			for (int i = 0; i < max; ++i)
			{
				fprintf_s(target, "\"%s\",\n", order[i].first.c_str());
			}
			fprintf_s(target, "\"%s\"", order[max].first.c_str());
		}
		else
		{
			int max = order.size() - 1;
			for (int i = 0; i < max; ++i)
			{
				fprintf_s(target, "\"%s\",", order[i].first.c_str());
			}
			fprintf_s(target, "\"%s\"", order[max].first.c_str());
		}
		//输出右侧括号
		fprintf_s(target, "]}");
		//关闭文件
		fclose(target);
		order.clear();
		target = NULL;
		return return_TypeDef::normal;
	}
	else
	{
		//分片逻辑

		//用于存放文件名逻辑的字符串
		char* pathexpr = NULL;
		int pathexpr_size = 0;
		//存放标号长度
		int length = 0;

		//准备写入config
		{
			//打开文件出错
			if (fopen_s(&target, (path + filename + (ext_name.size() ? ("config." + ext_name) : "config")).c_str(), "wb") != 0)
				return return_TypeDef::path_error;
			//定位到文件头
			fseek(target, 0, SEEK_SET);
			int size = _str_ids.size(),
				num = 0;
			//计算文件数
			num = size / pieces;
			if (size % pieces)
				++num;
			//计算序号长度
			for (int i = num; i; (i >>= 4), ++length);
			//写入 config 信息
			fprintf_s(target,
				"{\"offset\":%d,\n\"num\":%d,\n\"size\":%d,\n\"length\":%d,\n\"pieces\":%d}",
				_init_id, num, size, length, pieces);

			//关闭 config
			fclose(target);
			target = NULL;

			//写入 pathexpr 信息
			// 
			//tempc为确定文件名长度的字符串
			char tempc[10] = { 0 };
			sprintf_s(tempc, "%%0%dX", length);
			//temps为文件名格式化字符串
			std::string temps = path + filename+tempc;
			if (ext_name.size())
				temps += "." + ext_name;
			pathexpr_size = temps.size() + 3;
			pathexpr = new char[pathexpr_size];
			//如果申请失败报错
			if (pathexpr == NULL)
				return return_TypeDef::unexpected_error;
			//拷贝表达式
			strcpy_s(pathexpr, pathexpr_size, temps.c_str());
		}

		//主输出功能
		//if(0)
		{
			//存放正式文件名的空间
			char* cur_target_p = NULL;
			int  cur_target_sz = pathexpr_size + length+3;

			cur_target_p = new char[cur_target_sz];
			//如果申请失败报错
			if (cur_target_p == NULL)
				return return_TypeDef::unexpected_error;

			//排序准备输出
			std::vector<std::pair<std::string, Str_ID_TypeDef>> order;
			prepare_list(_str_ids, order);

			//循环输出主程序
			int to_out = order.size();
			for (int filecnt = 0,cnt_id = 0; to_out > 0; ++filecnt, to_out -= pieces)
			{
				//生成当前文件名
				sprintf_s(cur_target_p, cur_target_sz, pathexpr, filecnt);

				FILE* cur_target = NULL;
				//打开文件出错
				if (fopen_s(&cur_target, cur_target_p, "wb") != 0)
					return return_TypeDef::path_error;
				//定位到文件头
				fseek(cur_target, 0, SEEK_SET);
				fprintf_s(cur_target, "[");
				//生成循环结束条件
				int stop = (to_out > pieces ? pieces : to_out) - 1;
				//根据换行条件循环输出id的前n字节
				if (line)
					for (int i = 0; i < stop; ++i, ++cnt_id)
					{
						fprintf_s(cur_target, "\"%s\",\n",order[cnt_id].first.c_str());
					}
				else
					for (int i = 0; i < stop; ++i, ++cnt_id)
					{
						fprintf_s(cur_target, "\"%s\",",order[cnt_id].first.c_str());
					}
				//单独输出最后一个
				fprintf_s(cur_target, "\"%s\"]", order[cnt_id].first.c_str());
				++cnt_id;
				//关闭文件
				fclose(cur_target);
			}


			//释放文件名空间
			delete[] cur_target_p;
			//释放数组空间
			order.clear();
		}


		//释放表达式占用的内存
		delete[] pathexpr;
	}
	return return_TypeDef::normal;
}

int Str_ID_Mgr::Load(const std::string& file_path)
{
	return -1;
}