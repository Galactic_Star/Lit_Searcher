// 可用方法及功能
// object file_server.getAuthorByID(int id)                 通过作者ID查询作者对象（可能为string 或object），查无返回null
// object file_server.getArticleByID(int id)                通过文章ID查询文章全部信息，查无返回null
// Array file_server.getCooperatorByID(int id)              通过作者ID查询有合作关系的其他合作者id列表，查无返回null
// Array file_server.getArticleByAuthorID(int id)           通过作者ID查询发布过的文献id列表，查无返回null
// Array/int file_server.getArticleIdByTitle(string str)    通过文献名称查询文献ID，查无返回null，查到多个则返回Array
// int file_server.getAuthoridByName (string str)           通过作者名称查询作者ID，查无返回null，
// file_server.author_cnt.maxid[i]                          文献数量top100中的最高第 i+1 个的id（i从0开始）
// file_server.author_cnt.maxnum[i]                         文献数量top100中的最高第 i+1 个的文献数量（i从0开始）
// file_server.
// 
// 


function file_server_t(data)
{
    this.filecount=data.filecount;
    this.authorcount=data.authorcount;
    this.pieces=data.pieces;
    this.offset = data.offset;
    this.Articles=data.Articles;
    this.Book=data.Book;
    this.Inproceedings=data.Inproceedings;
    this.Mastersthesis=data.Mastersthesis;
    this.Phdthesis=data.Phdthesis;
    this.Proceedings=data.Proceedings;
    this.WWWs=data.WWWs;
    this.Incollections=data.Incollections;
    this.authorfreq=data.authorfreq;
}

function my_file_server_init(data)
{
    console.log("Storage support:\n\n\
      HIHIHIHI          HIHI        HIHI            HIHI          HIHIHIHI     HIHIHIHIHI   HIHI     HIHIHIHI\n\
    HIHI    HIHI       HIHIHI       HIHI           HIHIHI       HIHI    HIHI      HIHI      HIHI   HIHI    HIHI\n\
    HIHI              HIHIHIHI      HIHI          HIHIHIHI      HIHI              HIHI      HIHI   HIHI        \n\
    HIHI  HIHIHI     HIHI  HIHI     HIHI         HIHI  HIHI     HIHI              HIHI      HIHI   HIHI        \n\
    HIHI    HIHI    HIHI    HIHI    HIHI        HIHI    HIHI    HIHI    HIHI      HIHI      HIHI   HIHI    HIHI\n\
      HIHIHIHIHI  HIHIHIHI   HIHI   HIHIHIHI  HIHIHIHI   HIHI    HIHIHIHIHI       HIHI      HIHI     HIHIHIHIHI ");
    $.ajaxSettings.async = false;
    window.file_server = new file_server_t(data);
    var temp = ($.getJSON("resource/Bconfig.json"));
    //while(temp.statusText !="OK");
    file_server.author_info = temp.responseJSON;

    file_server.author_cnt = ($.getJSON("resource/Cinfo.json")).responseJSON;

    //通过作者ID查询作者名称
    file_server_t.prototype.getAuthorByID = function(id)
    {
        if(id < this.author_info.offset)
        {
            return null;
        }
        //找到定位标
        var pos = id - this.author_info.offset;
        // 超出上界
        if(pos >= this.author_info.size)
        {
            return null;
        }
        var pfile = ((Math.floor(pos/this.author_info.pieces)).toString(16).toUpperCase());
        for(var i = pfile.length;i<this.author_info.length;++i)
        {
            pfile = "0"+pfile;
        }
        var temp = ($.getJSON("resource/B"+pfile+".json"));
        //while(temp.statusText !="OK");
        var rst = temp.responseJSON;
        return rst[pos%this.author_info.pieces];
    };


    
    //通过文章ID查询文章全部信息
    //文章id没有offset
    file_server_t.prototype.getArticleByID = function(id)
    {
        if(id < 0)
        {
            return null;
        }
        // 超出上界
        if(id >= this.filecount)
        {
            return null;
        }
        var pfile = ((Math.floor(id/this.pieces)).toString(16).toUpperCase());
        var temp = ($.getJSON("resource/A"+pfile+".json"));
        //while(temp.statusText !="OK");
        var rst = temp.responseJSON;
        return rst[id%this.pieces];
    };

    
    //通过作者ID查询有合作关系的其他合作者id
    file_server_t.prototype.getCooperatorByID = function(id)
    {
        if(id < this.author_info.offset)
        {
            return null;
        }
        //找到定位标
        var pos = id - this.author_info.offset;
        // 超出上界
        if(pos >= this.author_info.size)
        {
            return null;
        }
        var pfile = ((Math.floor(pos/this.author_info.pieces)).toString(16).toUpperCase());
        for(var i = pfile.length;i<this.author_info.length;++i)
        {
            pfile = "0"+pfile;
        }
        var temp = ($.getJSON("resource/C"+pfile+".json"));
        //while(temp.statusText !="OK");
        var rst = temp.responseJSON;
        return rst.cop[pos%this.author_info.pieces];
    };

    
    //通过作者ID查询文章ID列表
    file_server_t.prototype.getArticleByAuthorID = function(id)
    {
        if(id < this.author_info.offset)
        {
            return null;
        }
        //找到定位标
        var pos = id - this.author_info.offset;
        // 超出上界
        if(pos >= this.author_info.size)
        {
            return null;
        }
        var pfile = ((Math.floor(pos/this.author_info.pieces)).toString(16).toUpperCase());
        for(var i = pfile.length;i<this.author_info.length;++i)
        {
            pfile = "0"+pfile;
        }
        var temp = ($.getJSON("resource/C"+pfile+".json"));
        //while(temp.statusText !="OK");
        var rst = temp.responseJSON;
        return rst.art[pos%this.author_info.pieces];
    };

    
    //通过文章title查询文章ID
    file_server_t.prototype.getArticleIdByTitle = function(str)
    {
        var pfile = BKDRHash12(str).toString(16).toUpperCase();
        var temp = ($.getJSON("resource/R"+pfile+".json"));
        var rst = temp.responseJSON;
        temp = rst[ BKDRHash32(str).toString(10)];
        if(temp)
        {
            if(Array.isArray(temp))
            {
                if(temp.length == 1)
                {
                    return temp[0];
                }
                var ret = new Array();
                //console.log(file_server.getArticleByID(temp[737]));
                for(var i = 0;i<temp.length;++i)
                {
                    var tt =file_server.getArticleByID(temp[i]);
                    //console.log(tt);
                    if((tt.title === str)||(tt.booktitle === str)||(tt.title === str))
                    {
                        console.log(temp[i]);
                        ret.push( temp[i]);
                    }
                }
                if(ret.length ==1)
                    return ret[0];
                else
                    return ret;
            }
            else
            return temp;
        }
        else return null;
    };

    
    //通过作者名查询作者ID
    file_server_t.prototype.getAuthoridByName = function(str)
    {
        var pfile = BKDRHash12(str).toString(16).toUpperCase();
        var temp = ($.getJSON("resource/U"+pfile+".json"));
        var rst = temp.responseJSON;
        temp = rst[ BKDRHash32(str).toString(10)];
        if(temp)
        {
            if(Array.isArray(temp))
            {
                if(temp.length == 1)
                {
                    return temp[0];
                }
                for(var i = 0;i<temp.size;++i)
                {
                    var tt = file_server.getAuthorByID(temp[i]).value;
                    if((tt === str)||(tt.value === str))
                    {
                        return temp[i];
                    }
                }
            }
            else
            return temp;
        }
        else return null;
    };
}

$.getJSON("resource/info.json",my_file_server_init);