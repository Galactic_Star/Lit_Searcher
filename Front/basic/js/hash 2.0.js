// BKDR Hash Function
//传入参数类型为String
function BKDRHash(str) {
    var seed = 131;
    var hash = 0;
    var i = 0;
    while (true) {
        var ch = str.charCodeAt(i++);
        if (!(ch > 0 || ch < 0)) {
            break;
        }
        hash = (seed * hash + ch) & 0x7FFFFFFF;
    }
    return hash;
};
function BKDRHash8(str) {
    var seed = 1;
    var hash = 0;
    var i = 0;
    while (true) {
        var ch = str.charCodeAt(i++);
        if (!(ch > 0 || ch < 0)) {
            break;
        }
        hash = ((hash << seed) + ch) & 0xFF;
    }
    return hash;
};
function BKDRHash12(str) {
    var seed = 2;
    var hash = 0;
    var i = 0;
    while (true) {
        var ch = str.charCodeAt(i++);
        if (!(ch > 0 || ch < 0)) {
            break;
        }
        hash = ((hash << seed) + ch) & 0xFFF;
    }
    return hash;
};
function BKDRHash16(str) {
    var seed = 3;
    var hash = 0;
    var i = 0;
    while (true) {
        var ch = str.charCodeAt(i++);
        if (!(ch > 0 || ch < 0)) {
            break;
        }
        hash = ((hash << seed) + ch) & 0xFFFF;
    }
    return hash;
};
function BKDRHash32(str) {
    var seed = 4;
    var hash = 0;
    var i = 0;
    while (true) {
        var ch = str.charCodeAt(i++);
        if (!(ch > 0 || ch < 0)) {
            break;
        }
        hash = ((hash << seed) + ch) & 0xFFFFFFFF;
    }
    return hash;
};