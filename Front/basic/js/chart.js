//测试用例
var chartTest = [
    { author: "牛二", number: 435 },
    { author: "张三", number: 382 },
    { author: "李四", number: 280 },
    { author: "王五", number: 262 }
];
var chartTest_name = new Array();
var chartTest_number = new Array();
for (let i = 0; i < chartTest.length; i++) {
    chartTest_name[i] = chartTest[i].author;
    chartTest_number[i] = chartTest[i].number;
}


//myChart函数输出写文章最多的前n名作者
//author:作者名数组,articleNum:作者文章数
function myChart(author,articleNum) {
    'use strict';

    var type = 'bar';
    var data = {
        // labels: [2014, 2015, 2016, 2017, 2018],//横轴标签
        labels: author,    //横轴标签
        datasets: [
            // {
            //     label: '优酷',
            //     data: [100, 200, 400, 800, 600],
            //     backgroundColor: '#4572a7'
            // },
            {
                label: '作者',
                //  data: [50, 100, 200, 400, 600]  //纵轴数值
                data: articleNum,  //纵轴数值
                backgroundColor: '#4572a7'
            }
        ]
    };

    var options = {
        scales: {
            yAxes: [{
                ticks: {
                    // suggestedMin: 0,
                    // suggestedMax: 500,
                    stepSize: 50,
                }
            }]
        },
        title: {
            display: true,
            text: '文献数(篇)',
            fontColor: '#333333',
            fontSize: '15',
            position: 'left'
        },
        legend: {
            position: 'left',
            display: true
        },
        //在柱状图上加显示数字
        "animation": {
            "duration": 1,
            "onComplete": function() {
              var chartInstance = this.chart,
                ctx = chartInstance.ctx;
      
              ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
              ctx.textAlign = 'center';
              ctx.textBaseline = 'bottom';
      
              this.data.datasets.forEach(function(dataset, i) {
                var meta = chartInstance.controller.getDatasetMeta(i);
                meta.data.forEach(function(bar, index) {
                  var data = dataset.data[index];
                  ctx.fillText(data, bar._model.x, bar._model.y - 5);
                });
              });
            }
          },
    };


    var ctx = document.getElementById('myChart').getContext('2d');
    var chart = new Chart(ctx, {
        type: type,
        data: data,
        options: options
    });

}

myChart(chartTest_name,chartTest_number);