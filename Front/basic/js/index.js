
//为了让多个函数在网页加载完毕后立刻执行，引入该函数
function addLoadEvent(func) {
    var oldonload = window.onload;
    if (typeof window.onload != 'function') {
        window.onload = func;
    }
    else {
        window.onload = function () {
            oldonload();
            func();
        }
    }
}

//当搜索按钮被点击时，获得关键词类型key_words_typ和关键词key_words
document.getElementById("search_button").onclick=function(){
    // alert(1230);
    let myselect=document.getElementById("search_input_select");
    let index=myselect.selectedIndex;
    let key_words_type = myselect.options[index].text;
    alert(key_words_type);
    
}

//addLoadEvent(myChart);//调用
//addLoadEvent(tanchuang);调用



//注意调用方式!!!!!!!!!!!!!!!!!!!!!!!!!
//函数调用
//search_action();

