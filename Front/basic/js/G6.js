//Tooltip插件,作用是高亮相邻节点,
const tooltip = new G6.Tooltip({
  offsetX: 10,
  offsetY: 10,
  fixToNode: [1, 0.5],
  // the types of items that allow the tooltip show up
  // 允许出现 tooltip 的 item 类型
  itemTypes: ['node', 'edge'],
  // custom the tooltip's content
  // 自定义 tooltip 内容
  getContent: (e) => {
    const outDiv = document.createElement('div');
    outDiv.style.width = 'fit-content';
    outDiv.style.height = 'fit-content';
    const model = e.item.getModel();
    if (e.item.getType() === 'node') {//若选中了节点
      outDiv.innerHTML = `合作文章:...${model.label}`;//展示某些信息
    } 
    //不用展示边的去向（因为是无向图）
    // else {
    //   const source = e.item.getSource();
    //   const target = e.item.getTarget();
    //   outDiv.innerHTML = `来源：${source.getModel().name}<br/>去向：${target.getModel().name}`;
    // }
    return outDiv;
  },
});

//minimap 插件
const minimap = new G6.Minimap({
  size: [100, 100],
  className: 'minimap',
  type: 'delegate',
});




// 实例化图
  const graph = new G6.Graph({
    container: 'mountNode',
    width: 1200,
    height: 600,
    // 节点默认配置
    defaultNode: {
      labelCfg: {
        style: {
          fill: '#black',
        },
      },
    },
    // 边默认配置
    defaultEdge: {
      labelCfg: {
        autoRotate: true,
      },
    },
    // 节点在各状态下的样式
    nodeStateStyles: {
      // hover 状态为 true 时的样式
      hover: {
        fill: 'lightsteelblue',
      },
      // click 状态为 true 时的样式
      click: {
        stroke: '#000',
        lineWidth: 3,
      },
    },
    // 边在各状态下的样式
    edgeStateStyles: {
      // click 状态为 true 时的样式
      click: {
        stroke: 'steelblue',
      },
    },
    // 布局
    layout: {
      type: 'force',//经典力导向布局
      linkDistance: 100,
      preventOverlap: true,//是否防止节点重叠
      nodeStrength: -30,
      edgeStrength: 0.1,
    },
    plugins: [tooltip],//插件
    // 内置交互
    modes: {
      default: [
        'drag-canvas',
         'zoom-canvas', 
         'drag-node',
         {
          type: 'activate-relations',
          resetSelected: true,
        },
    ],
    },
  });

  // graph.on('afteractivaterelations', (e) => {
  //   // 当前操作的节点 item
  //   //console.log(e.item);
  //   // 当前操作是选中(`'activate'`)还是取消选中(`'deactivate'`)
  //   //console.log(e.action);
  // });

   const main = async () => {
    //  const response = await fetch(
    //    'https://gw.alipayobjects.com/os/basement_prod/6cae02ab-4c29-44b2-b1fd-4005688febcb.json',
    //  );
    //  const remoteData = await response.json();
     const remoteData ={ //数据
      "nodes": [
        {"id": "0", "label": "n0", "class": "c0","x": 1000, "y": -100 },
        {"id": "1", "label": "n1", "class": "c0","x": 300, "y": -10 },
        {"id": "2", "label": "n2", "class": "c0","x": 10, "y": 10 },
        {"id": "3", "label": "n3", "class": "c0","x": 320, "y": -100 },
        {"id": "4", "label": "n4", "class": "c0","x": 100, "y": 900 },
        {"id": "5", "label": "n5", "class": "c0","x": 120, "y": 213 },
        {"id": "6", "label": "n6", "class": "c1","x": 543, "y": 12 },
        {"id": "7", "label": "n7", "class": "c1","x": 543, "y": -100 },
        {"id": "8", "label": "n8", "class": "c1","x": 1, "y": 0 },
        {"id": "9", "label": "n9", "class": "c1","x": 0, "y": -222 },
        {"id": "10", "label": "n10", "class": "c1","x": 435, "y": 69 },
        {"id": "11", "label": "n11", "class": "c1","x": 23, "y": 10 },
        {"id": "12", "label": "n12", "class": "c1","x": -129, "y": 39 },
        {"id": "13", "label": "n13", "class": "c2","x": 234, "y": 843 },
        {"id": "14", "label": "n14", "class": "c2","x": -301, "y": 129 },
        {"id": "15", "label": "n15", "class": "c2","x": -20, "y": -76 },
        {"id": "16", "label": "n16", "class": "c2","x": 1220, "y": -34 },
        {"id": "17", "label": "n17", "class": "c2","x": -10, "y": 954 },
        {"id": "18", "label": "n18", "class": "c2","x": 492, "y": 123 },
        {"id": "19", "label": "n19", "class": "c2","x": 123, "y": -241 }
      ],
      "edges": [
        {"source": "0", "target": "1", "label": "e0-1", "weight": 1 },
        {"source": "0", "target": "2", "label": "e0-2", "weight": 2 },
        {"source": "0", "target": "3", "label": "e0-3", "weight": 3 },
        {"source": "0", "target": "4", "label": "e0-4", "weight": 1.4 },
        {"source": "0", "target": "5", "label": "e0-5", "weight": 2 },
        {"source": "0", "target": "7", "label": "e0-7", "weight": 2 },
        {"source": "0", "target": "8", "label": "e0-8", "weight": 2 },
        {"source": "0", "target": "9", "label": "e0-9", "weight": 1.3 },
        {"source": "0", "target": "10", "label": "e0-10", "weight": 1.5 },
        {"source": "0", "target": "11", "label": "e0-11", "weight": 1 },
        {"source": "0", "target": "13", "label": "e0-13", "weight": 10 },
        {"source": "0", "target": "14", "label": "e0-14", "weight": 2 },
        {"source": "0", "target": "15", "label": "e0-15", "weight": 0.5 },
        {"source": "0", "target": "16", "label": "e0-16", "weight": 0.8 },
        {"source": "2", "target": "3", "label": "e2-3", "weight": 1 },
        {"source": "4", "target": "5", "label": "e4-5", "weight": 1.4 },
        {"source": "4", "target": "6", "label": "e4-6", "weight": 2.1 },
        {"source": "5", "target": "6", "label": "e5-6", "weight": 1.9 },
        {"source": "7", "target": "13", "label": "e7-13", "weight": 0.5 },
        {"source": "8", "target": "14", "label": "e8-14", "weight": 0.8 },
        {"source": "9", "target": "10", "label": "e9-10", "weight": 0.2 },
        {"source": "10", "target": "14", "label": "e10-14", "weight": 1 },
        {"source": "10", "target": "12", "label": "e10-12", "weight": 1.2 },
        {"source": "11", "target": "14", "label": "e11-14", "weight": 1.2 },
        {"source": "12", "target": "13", "label": "e12-13", "weight": 2.1 },
        {"source": "16", "target": "17", "label": "e16-17", "weight": 2.5 },
        {"source": "16", "target": "18", "label": "e16-18", "weight": 3 },
        {"source": "17", "target": "18", "label": "e17-18", "weight": 2.6 },
        {"source": "18", "target": "19", "label": "e18-19", "weight": 1.6 }
      ]
    }

    const nodes = remoteData.nodes;
    const edges = remoteData.edges;
    nodes.forEach((node) => {
      if (!node.style) {
        node.style = {};
      }
      node.style.lineWidth = 1;
      node.style.stroke = '#666';
      node.style.fill = 'steelblue';
      node.type = 'circle';
      node.size = Math.random() * 30 + 15;
    });
    edges.forEach((edge) => {
      if (!edge.style) {
        edge.style = {};
      }
      edge.style.lineWidth = edge.weight;
      edge.style.opacity = 0.6;
      edge.style.stroke = 'grey';
    });

    graph.data(remoteData);//给数据
    graph.render();//渲染


    

    // 监听鼠标进入节点
    graph.on('node:mouseenter', (e) => {
      const nodeItem = e.item;
      // 设置目标节点的 hover 状态 为 true
      graph.setItemState(nodeItem, 'hover', true);
    });
    // 监听鼠标离开节点
    graph.on('node:mouseleave', (e) => {
      const nodeItem = e.item;
      // 设置目标节点的 hover 状态 false
      graph.setItemState(nodeItem, 'hover', false);
    });
    // 监听鼠标点击节点
    graph.on('node:click', (e) => {
      // 先将所有当前有 click 状态的节点的 click 状态置为 false
      const clickNodes = graph.findAllByState('node', 'click');
      clickNodes.forEach((cn) => {
        graph.setItemState(cn, 'click', false);
      });
      const nodeItem = e.item;
      // 设置目标节点的 click 状态 为 true
      graph.setItemState(nodeItem, 'click', true);
      
    });
    // 监听鼠标点击边
    graph.on('edge:click', (e) => {
      // 先将所有当前有 click 状态的边的 click 状态置为 false
      const clickEdges = graph.findAllByState('edge', 'click');
      clickEdges.forEach((ce) => {
        graph.setItemState(ce, 'click', false);
      });
      const edgeItem = e.item;
      // 设置目标边的 click 状态 为 true
      graph.setItemState(edgeItem, 'click', true);
    });

    graph.on('node:dragstart', function (e) {
      graph.layout();
      refreshDragedNodePosition(e);
    });
    graph.on('node:drag', function (e) {
      const forceLayout = graph.get('layoutController').layoutMethods[0];
      forceLayout.execute();
      refreshDragedNodePosition(e);
    });
    graph.on('node:dragend', function (e) {
      e.item.get('model').fx = null;
      e.item.get('model').fy = null;
    });
    
    //选中之后邻居高亮相关代码
    function clearAllStats() {
      graph.setAutoPaint(false);
      graph.getNodes().forEach(function (node) {//清除所有点状态
        graph.clearItemStates(node);
      });
      graph.getEdges().forEach(function (edge) {//清除所有边状态
        graph.clearItemStates(edge);
      });
      graph.paint();
      graph.setAutoPaint(true);
    }
    
    graph.on('node:mouseenter', function (e) {
      const item = e.item;
      graph.setAutoPaint(false);
      graph.getNodes().forEach(function (node) {
        graph.clearItemStates(node);
        graph.setItemState(node, 'dark', true);
      });
      graph.setItemState(item, 'dark', false);
      graph.setItemState(item, 'highlight', true);
      graph.getEdges().forEach(function (edge) {
        if (edge.getSource() === item) {//若边的源是选中的节点，高亮
          graph.setItemState(edge.getTarget(), 'dark', false);
          graph.setItemState(edge.getTarget(), 'highlight', true);
          graph.setItemState(edge, 'highlight', true);
          edge.toFront();
        } else if (edge.getTarget() === item) {//若边的目标是选中的节点，也高亮
          graph.setItemState(edge.getSource(), 'dark', false);
          graph.setItemState(edge.getSource(), 'highlight', true);
          graph.setItemState(edge, 'highlight', true);
          edge.toFront();
        } else {//其他边不高亮
          graph.setItemState(edge, 'highlight', false);
        }
      });
      graph.paint();
      graph.setAutoPaint(true);
    });
    graph.on('node:mouseleave', clearAllStats);
    graph.on('canvas:click', clearAllStats);
    //选中之后邻居高亮相关代码结束


if (typeof window !== 'undefined')
  window.onresize = () => {
    if (!graph || graph.get('destroyed')) return;
    if (!container || !container.scrollWidth || !container.scrollHeight) return;
    graph.changeSize(container.scrollWidth, container.scrollHeight);
  };

function refreshDragedNodePosition(e) {
  const model = e.item.get('model');
  model.fx = e.x;
  model.fy = e.y;
}


  };
  main();




  //搜索结点函数,nodeValue为要搜索的节点的信息
  const searchNode=(nodeValue)=>{
  const findNode = graph.find('node', (node) => {
    return node.get('model').label === nodeValue;
  });
  
  const item = findNode;
  if(item===undefined)//假设未找到
    alert("未找到相关信息")
  graph.setAutoPaint(false);
  graph.getNodes().forEach(function (node) {
    graph.clearItemStates(node);
    graph.setItemState(node, 'dark', true);
  });
  graph.setItemState(item, 'dark', false);
  graph.setItemState(item, 'highlight', true);
  graph.getEdges().forEach(function (edge) {
    if (edge.getSource() === item) {
      graph.setItemState(edge.getTarget(), 'dark', false);
      graph.setItemState(edge.getTarget(), 'highlight', true);
      graph.setItemState(edge, 'highlight', true);
      edge.toFront();
    } else if (edge.getTarget() === item) {
      graph.setItemState(edge.getSource(), 'dark', false);
      graph.setItemState(edge.getSource(), 'highlight', true);
      graph.setItemState(edge, 'highlight', true);
      edge.toFront();
    } else {
      graph.setItemState(edge, 'highlight', false);
    }
  });
  graph.paint();
  graph.setAutoPaint(true);
  }

  // searchNode("n0");
  document.getElementById("g6_search_button").onclick=function(){
    let mySearch=document.getElementById("g6_search_input_box");
    let myValue=mySearch.value;
    searchNode(myValue);
}
